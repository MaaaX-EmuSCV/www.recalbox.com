
$(document).ready(function(){
    var location = window.location.href
    var found = false;
    $('.z-nav__link').each(function(){
        href = $(this).attr("href")
        if(href != "/" && href.substring(href.length-1) != "/" && location.indexOf(href) !== -1){
            $(this).addClass("z-nav__link--active")
            found = true;
        }
    })
    if(!found) $(".z-nav__link[href='/']").addClass("z-nav__link--active")
});