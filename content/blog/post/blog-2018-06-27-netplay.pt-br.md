+++
date = "2018-06-27"
image = "/images/blog/2018-06-27-netplay/recalbox-18.06.27-banner.jpg"
title = "Netplay"

[author]
  github = "https://github.com/Supernature2k"
  gitlab = "https://gitlab.com/Supernature2k"
  name = "Supernature2k"
[translator]
  github = "https://github.com/nwildner"
  gitlab = "https://gitlab.com/nwildner"
  name = "nwildner"

+++


Here comes a new challenger!!!

Já sonhou alguma vez em jogar online seus jogos retrô, desbloquear conquistas como você faz com seu console de próxima geração?

Bem... O Retroarch implementou e o Recalbox trouxe para você no estilo amigável ao usuário de sempre!


Para iniciar, vá para o menu / Opções de Jogos / Configurações de jogo em Rede


[{{< img src="/images/blog/2018-06-27-netplay/netplay-settings.png" alt="netplay settings">}}](/images/blog/2018-06-27-netplay/netplay-settings.png)



Daqui, você pode ativar/desativar o Jogar em Rede, configurar seu usuário, escolher uma porta.

Você também pode adicionar o hash CRC32 na sua lista de jogos para encontrar mais facilmente salas de Jogos em Rede:



[{{< img src="/images/blog/2018-06-27-netplay/hash.png" alt="hash">}}](/images/blog/2018-06-27-netplay/hash.png)



Note que pode levar um certo tempo numa lista de jogos grande (menos que a busca de dados - scrapping) mas é altamente recomentado para uma maior compatibilidade.
Você só pode adicionar hash a uma lista de jogos existente (significa que antes, a busca de dados deve ser efetuada).



Agora está tudo pronto!!!!

Na lista de jogos dos sistemas compatíveis (fba_libretro,mame,mastersystem,megadrive,neogeo,nes,pcengine,sega32x,sg1000,supergrafx) você pode iniciar qualquer jogo como *dono da sala* com o botão X ao invés de B

*Note que poderá haver problemas de performance com alguns destes núcleos*


[{{< img src="/images/blog/2018-06-27-netplay/x.png" alt="x">}}](/images/blog/2018-06-27-netplay/x.png)



Certo, podemos hospedar partidas mas como podemos ingressar em partidas existente? é fácil!!!

Da tela principal, basta pressionar X e a nova interface de Jogos em Rede com as partidas disponíveis será exibida!



[{{< img src="/images/blog/2018-06-27-netplay/lobby.png" alt="lobby">}}](/images/blog/2018-06-27-netplay/lobby.png)


Para cada jogo você verá bastante informação relevante:

* Usuário (com um pequeno ícone se o jogo for iniciado pelo Recalbox)
* País
* Hash OK (caso você possua a mesma rom com o mesmo hash em seus jogos)
* Arquivo OK
* Núcleo
* Latência da conexão e outras informações

Você verá um *resultado* na parte inferior (e em frente ao nome do jogo na lista) dizendo qual o grau de compatibilidade para que você possa ingressar na partida:

* Verde: você possui uma rom boa com uma hash boa, núcleo ok, quase certo que irá funcionar 
* Azul: hash não coincide (algumas roms não possuem hash ex. arcade) mas o arquivo está no seu sistema e o núcleo está ok. *deverá* funcionar
* Vermelho: Arquivo não encontrado, sistema não autorizado, núcleos não coincidem: sem chance de funcionar (jogo não será executado)

Ready Player One?
