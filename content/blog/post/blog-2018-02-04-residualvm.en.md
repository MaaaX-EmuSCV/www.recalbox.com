+++
date = "2018-02-04T07:00:00+00:00"
image = "/images/blog/2018-02-04-residualvm/recalbox-residualvm-banner.png"
title = "ResidualVM"

[author]
  github = "https://github.com/rockaddicted"
  gitlab = "https://gitlab.com/rockaddicted"
  name = "rockaddicted"
+++

Hi pals,  

Today a new emulated system is available on Recalbox : **ResidualVM !**  

It is a sister project of **ScummVM**, that allows you to play to games using **GrimE** game engine. Three games are compatibles with this emulator:  
**Grim Fandango**, **Escape from Monkey Island** and **Myst III - Exile**.  

This new emulator has been added by [lmerckx](https://gitlab.com/lmerckx) as a **second ScummVM core**. The switch between ScummVM and ResidualVM is automatic. Indeed, the way to add new games is the same as for ScummVM. The only difference is the file extension to use. Once your game inside the **/recalbox/roms/scummvm** folder, you have to name your shortcut file as **gameShortName.residualvm** instead of **gameShortName.scummvm** (for example **grim.residualvm** to add Grim Fandango).  
Recalbox will automatically select and use the right emulator.  

If you need more information about this new emulator, please read the [ResidualVM documentation](http://www.residualvm.org/documentation/).  
You will find the **compatibility list** [there](http://www.residualvm.org/compatibility/) as well.  


We hope you'll enjoy this new emulator and (re)discover the **ResidualVM / GrimE** games library.  

## Grim Fandango
[![grim01](/images/blog/2018-02-04-residualvm/grim01-thumb.jpg)](/images/blog/2018-02-04-residualvm/grim01.jpg)
[![grim02](/images/blog/2018-02-04-residualvm/grim02-thumb.jpg)](/images/blog/2018-02-04-residualvm/grim02.jpg)
[![grim03](/images/blog/2018-02-04-residualvm/grim03-thumb.jpg)](/images/blog/2018-02-04-residualvm/grim03.jpg)

## Escape from Monkey Island
[![monkey01](/images/blog/2018-02-04-residualvm/monkey01-thumb.jpg)](/images/blog/2018-02-04-residualvm/monkey01.jpg)
[![monkey02](/images/blog/2018-02-04-residualvm/monkey02-thumb.jpg)](/images/blog/2018-02-04-residualvm/monkey02.jpg)
[![monkey03](/images/blog/2018-02-04-residualvm/monkey03-thumb.jpg)](/images/blog/2018-02-04-residualvm/monkey03.jpg)

## Myst III - Exile
[![myst01](/images/blog/2018-02-04-residualvm/myst01-thumb.jpg)](/images/blog/2018-02-04-residualvm/myst01.jpg)
[![myst02](/images/blog/2018-02-04-residualvm/myst02-thumb.jpg)](/images/blog/2018-02-04-residualvm/myst02.jpg)
[![myst03](/images/blog/2018-02-04-residualvm/myst03-thumb.jpg)](/images/blog/2018-02-04-residualvm/myst03.jpg)
