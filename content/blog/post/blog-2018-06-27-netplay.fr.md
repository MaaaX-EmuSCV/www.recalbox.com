+++
date = "2018-06-27"
image = "/images/blog/2018-06-27-netplay/recalbox-18.06.27-banner.jpg"
title = "Netplay"

[author]
  github = "https://github.com/Supernature2k"
  gitlab = "https://gitlab.com/Supernature2k"
  name = "Supernature2k"
[translator]
  github = "https://github.com/substring"
  gitlab = "https://gitlab.com/substring45"
  name = "Substring"
+++


Here comes a new challenger!!!

Avez-vous déjà rêvé de jouer à vos jeux retro en ligne, débloquer des succès comme sur les consoles next gen ?

Et bien Retroarch l'a fait, et Recalbox l'intègre avec une interface simple comme toujours !


Pour commencer, rendez vous dans le menu Options des jeux / Options netplay

[{{< img src="/images/blog/2018-06-27-netplay/netplay-settings.png" >}}](/images/blog/2018-06-27-netplay/netplay-settings.png)



De là, vous pouvez activer ou non le netplay, définir votre nom en ligne et choisir un port

Vous pouvez aussi hasher vos roms (empreinte numérique unique) pour une meilleure reconnaissance lors du choix d'une partie



[{{< img src="/images/blog/2018-06-27-netplay/hash.png" alt="hash">}}](/images/blog/2018-06-27-netplay/hash.png)



Notez bien que sur les gros romsets, ça peut être un peu lent (mais beaucoup moins que le scrape hein! ). Hasher est quand même fortement recommandé pour garantir de trouver des parties sur les mêmes roms que vous.
Attention : seules les roms scrapées peuvent être hashées !



Maintenant, vous êtes prêts !!!!!

Parmi la liste des systèmes permis en netplay (fba_libretro,mame,mastersystem,megadrive,neogeo,nes,pcengine,sega32x,sg1000,supergrafx) vous pouvez maintenant démarrer une partie en tant qu'hébergeur en appuyant sur X au lieu de B

*Certains cores peuvent être très exigeants niveaux performances, voire trop, et nuire à l'expérience en ligne*


[{{< img src="/images/blog/2018-06-27-netplay/x.png" alt="x">}}](/images/blog/2018-06-27-netplay/x.png)



Vous savez comment lancer un jeu en hôte, mais comment rejoindre une partie ? Facile !!!

Depuis l'écran principale avec la liste des systèmes, appuyez simplement sur X qui vous proposera une interface listant les parties en cours !



[{{< img src="/images/blog/2018-06-27-netplay/lobby.png" alt="lobby">}}](/images/blog/2018-06-27-netplay/lobby.png)


Pour chaque jeu, vous aurez des informations :

* Utilisateur (Avec une petite icône Recalbox quand c'est une partie lancée depuis Recalbox)
* Pays
* Correspondance du hash (si vous avez une rom avec le même hash)
* Correspondance de fichier
* Core
* Latence et autres infos

Vous verrez le résultat dans le bas de la fenêtre (et devant chaque item de la liste) vous indiquant les chances de succès de connexion :

* Vert : vous avez la bonne rom avec le bon hash, le bon core, toutes les chances sont de votre côté pour que ca marche !
* Bleu : pas de correspondance du hash (certaines roms n'ont pas de hash, comme les systèmes arcade), mais le bon fichier a été trouvé. Ca *devrait* marcher
* Rouge : fichier non trouvé, système interdit, pas le bon core : aucune chance que le jeu en ligne marche (rien ne sera lancé)

Ready Player One?
