+++
date = "2017-11-10T20:00:00+00:00"
image = "/images/blog/2017-11-10-hyperion/recalbox-hyperion-banner.jpg"
title = "Faites briller votre Recalbox avec Hyperion!"

[author]
  github = "https://github.com/digitallumberjack"
  gitlab = "https://gitlab.com/digitallumberjack"
  name = "digitalLumberjack"
  twitter = "https://twitter.com/digitalumberjak"

+++

**Hyperion** (https://github.com/hyperion-project/hyperion) est un projet libre et open source pour transformer votre RaspberryPi en système **Ambilight** !

Avec Hyperion et recalbox, vous allez transformer votre système de retrogaming et multimedia system en une magnifique experience immersive !


## I - Description

Tout d'abord, nous ne parlons pas d'utiliser Hyperion avec une partie de votre recalbox comme **Kodi**. 
Vous serez capable d'utiliser les fonctions d'Hyperion dans **CHACUN DES JEUX RETRO** auquel vous jouez sur Recalbox !

![Hyperion avec recalbox](/images/blog/2017-11-10-hyperion/pukerainbow.gif)

L'image que vous voyez sur votre TV (LCD ou CRT) sera **dynamiquement étendu** sur le mur avec des leds RGB.

A quoi ça ressemble? Regardez avec Sonic 3 sur Sega Megadrive/Genesis:

[![Sonic Title](/images/blog/2017-11-10-hyperion/sonic1-thumb.jpg)](/images/blog/2017-11-10-hyperion/sonic1.jpg)
[![Sonic Plane](/images/blog/2017-11-10-hyperion/sonic2-thumb.jpg)](/images/blog/2017-11-10-hyperion/sonic2.jpg)
[![Sonic Stage 1](/images/blog/2017-11-10-hyperion/sonic3-thumb.jpg)](/images/blog/2017-11-10-hyperion/sonic3.jpg)

Et en vidéo:

<iframe width="560" height="315" src="https://www.youtube.com/embed/z1QblkdO6bs" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## II - Materiel

Hyperion supporte de nombreuses bandes de LED (voir https://hyperion-project.org/wiki/Supported-hardware) avec differents protocoles: 

* **SPI**: LPD6803, LPD8806, APA102, WS2812b, and WS2801 
* **USB**: AdaLight et SEDU
* **PWM**: WS2812b

Les tests ont été faits sur **WS2812b** avec PWM et SPI. Je n'ai jamais réussi à faire fonctionner PWM correctement, nous allons donc décrire comment faire votre installation recalbox/hyperion avec le mode **WS2812b en SPI**. Cette section est absente du wiki Hyperion, nous allons donc expliquer comment l'utiliser ici (et mettre à jour le wiki Hyperion avec ces informations).

La prise en charge de **WS2812b** avec un fil sur SPI a été ajoutée par [penfold42](https://github.com/penfold42) avec [ce commit](https://github.com/hyperion-project/hyperion/commit/a960894d140405e29edb413685e700d2f1714212). Merci.

Le tutoriel suivant utilise un fer à souder. Si vous en possedez déjà un, le reste du matériel est plutôt bon marché. Mais si vous souhaitez faire quelque chose sans soudure, voir le tutoriel ici :
https://hyperion-project.org/threads/raspberry-pi-3-mediacenter-hyperion-ambilight-no-soldering.77/


Vous aurez besoin de:

* votre recalbox
* une bande de led WS2812b [<i class="fa fa-picture-o"></i>](/images/blog/2017-11-10-hyperion/ws2812b.jpg)
* une résistance de 330 Ohms à mettre sur le pin data 
* quelque chose pour couper la bande [<i class="fa fa-picture-o"></i>](/images/blog/2017-11-10-hyperion/pince.jpg)
* un fer à souder
* des fies ou des connecteurs pour connecter la bande de leds [<i class="fa fa-picture-o"></i>](/images/blog/2017-11-10-hyperion/connectors-item.jpg)
* un level shifter pour passer le voltage de data à 5V [<i class="fa fa-picture-o"></i>](/images/blog/2017-11-10-hyperion/voltage.jpg), ou un regulateur de tension [<i class="fa fa-picture-o"></i>](/images/blog/2017-11-10-hyperion/shifter.jpg) pour réduire le VCC des leds.
* des jumpers dupont pour connecter facilement les fils sur le RaspberryPi [<i class="fa fa-picture-o"></i>](/images/blog/2017-11-10-hyperion/dupont.jpg)
* une alimentation de 4A (min) 5v [<i class="fa fa-picture-o"></i>](/images/blog/2017-11-10-hyperion/powersupply.jpg)

N'hésitez pas à visiter le [<img style="vertical-align: baseline;" src="/images/shared/recalstore.png"/>](https://www.recalbox.com/shop) pour trouver tout ce dont vous avez besoin.


Bien sûr si vous voulez économiser vous pouvez : 

* souder directement (pas de connecteurs ni dupont)
* Utilisez une alimentation 3A. J'ai testé ma bande de 220 led hyperion avec une Aukru 5V 3A et ça fonctionnait presque bien, mais quand toutes les leds sont blanches, la fin de la bande est un peu jaune...


## III - Câblage

#### Leds

Mesurez les bords de votre TV et coupez 1 bande par bord selon cette mesure.

Soudez les bandes ensemble OU soudez des connecteurs sur les bandes. Suivez les flêches sur la bande pour savoir quel connecteur utiliser (IN ou OUT).

Voici un apercu du resultat:

[![connectors](/images/blog/2017-11-10-hyperion/connectors-thumb.jpg)](/images/blog/2017-11-10-hyperion/connectors.jpg)
[![connectors](/images/blog/2017-11-10-hyperion/solder-thumb.jpg)](/images/blog/2017-11-10-hyperion/solder.jpg)



#### Le système

Pour autoriser le Rpi à envoyer des données aux LEDs, vous avez le choix: 

* Baisser le voltage LED à 4.7V avec un convertisseur
* Redresser le signal du GPIO à 5V

###### A - Régulateur de tension

[![Circuit voltage regulator](/images/blog/2017-11-10-hyperion/wiring-regulator.png)](/images/blog/2017-11-10-hyperion/wiring-regulator.png)

(image originale du [wiki hyperion](https://hyperion-project.org/wiki/3-Wire-PWM))

Si vous êtes sur RPI2 ou 3, vous avez plus de GPIO mais le placement reste le même.

Le RPi est connecté sur sa propre alimentation et sur la masse de l'alimentation des LEDs.

Le RPi envoie les données à travers le SPI MOSI GPIO vers le câble données des LEDs. 

Le régulateur est connecté entre les LEDs et leur alimentation.

###### B - Level shifter (non testé)


Si vous choisissez de réguler le voltage des GPIO, utilisez le schema au dessus, enlevez le régulateur et connectez le level shifter entre le GPIO MOSI et le câble données des LEDs. 


## IV - Configuration 

#### Hypercon

**Hypercon** est une interface graphique qui vous permet de configurer votre installation Hyperion et créer le fichier de configuration a placer sur la recalbox.

Téléchargez et démarrez **Hypercon** en suivant le tutoriel officiel: https://hyperion-project.org/wiki/HyperCon-Information

[![hypercon screenshot](/images/blog/2017-11-10-hyperion/hypercon-thumb.jpg)](/images/blog/2017-11-10-hyperion/hypercon.jpg)


Vous trouverez de nombreuses ressources sur la confiuguration d'Hypercon sur le wiki Hyperion: https://hyperion-project.org/wiki/HyperCon-Guide

Voici comment configurer Hypercon pour votre recalbox et le **WS2812b en mode spi**:

* **Hardware**
 * Type -> WS281X-SPI
 * RGB Byte Order -> GRB
 * Construction and Image Process -> configure depending of your installation
 * Blackborder Detection -> Enabled 
     * Threshold -> 5
     * mode -> osd
* **Process**
  * _Smoothing_
     * Enabled -> True
     * Time -> 200ms
     * Update Freq -> 20
     * Update Delay -> 0
* **Grabber**
  * _Internal Frame Grabber_
     * Enabled -> True
     * Width -> 64
     * Height -> 64
     * Interval -> 200
     * Priority Channel -> 890
  * _GrabberV4L2_
     * Enabled -> False
* External
  * Booteffect / Static Color -> configure the boot effect you like here!
  
  
Cliquez ensuite sur le bouton **Create Hyperion Configuration** et sauvegardez le fichier json en `hyperion.config.json`.

Copiez ce fichier sur votre recalbox (en ssh ou par le partage réseau) dans `/recalbox/share/system/config/hyperion/hyperion.config.json` (`config/hyperion/hyperion.config.json` sur le réseau)


#### Activer le service Hyperion dans recalbox.conf

Activez simplement Hyperion dans recalbox.conf (voir [le wiki](https://github.com/recalbox/recalbox-os/wiki/recalbox.conf-%28EN%29)):  
Changez la ligne `hyperion.enabled=0` en `hyperion.enabled=1`

Modifiez le fichier config.txt sur la partition boot et ajoutez la ligne  `dtparam=spi=on`:

```bash
mount -o remount,rw /boot && echo 'dtparam=spi=on' >> /boot/config.txt
```

Redémarrez la recalbox.


## V - Profitez de l'arc en ciel

Vous avez un système ambilight totalement fonctionnel! Testez des jeux retro, des films et animes avec Kodi et laissez la beauté des couleurs améliorer votre expérience multimedia!

![](/images/blog/2017-11-10-hyperion/nyan.jpg)
