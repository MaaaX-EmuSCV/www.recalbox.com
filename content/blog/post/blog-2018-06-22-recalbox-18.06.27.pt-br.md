+++
date = "2018-06-27T01:00:00+02:00"
image = "/images/blog/2018-06-27-recalbox-18-06-27/recalbox-18.06.27-banner.png"
title = "Recalbox 18.06.27"

[author]
  github = "https://github.com/substring"
  gitlab = "https://gitlab.com/substring45"
  name = "Substring"
[translator]
  github = "https://github.com/nwildner"
  gitlab = "https://gitlab.com/nwildner"
  name = "nwildner"


+++

Olá a todos!

Um bom tempo se passou deste o último lançamento ... E sabem porque levou tanto tempo para uma nova versão? Porque estávamos trabalhando duro numa funcionalidade muito especial e que estava sendo integrada "do modo Recalbox"! Sim jovens! Aposto que vocês sabem o que **netplay** significa, não é mesmo? Está integrado ao ES em uma interface fácil de usar!

Vamos então aos destaques deste changelog:

* **Adicionanda introRecalboxEASports.mp4**: Com a Copa do Mundo FIFA, achamos que seria legal adicionar a versão Recalbox da famosa intro da EA ;)

* **ES: Adicionada a Opção Netplay aos menus + menus para adicionar rom hashs mas listas de jogos:** esta alteração engloba a configuração básica do netplay

* **ES: Adicionado o Netplay : Interface para modo cliente e botão para iniciar como host**: tudo isto estará detalhado na wiki + postagem específica sobre o netplay. Resumindo: uma bela janela irá listar jogos netplay existentes para você ingressar, com ícones indicando se você pode ou não se juntar a partida. Obviamente, iniciar um servidor também ficou simples! Memorize o X (layout do SNES) como o botão para chamar tudo que for relacionado ao netplay! Lembre-se também dos limites: sem netplay para consoles portáteis ou consoles 3D + alguns núcleos da libretro que são mais exigentes (CPU/RAM) para trabalhar com o Pi (ex: nes9x não irá iniciar o modo netplay no pi3 devido a falta de RAM)

* **ES: Corrigido o relógio fora do horário local**: exibia o fuso GMT, não interessando onde você mora

* **ES: Corrigido temas antigos com problema**: nossa culpa, perdão, o ES dava pau ao tentar alterar temas usando as especificações antigas

* **ES: Opção para a posição da popup**: você decide em qual canto da tela exibir!

* **ES: Mensagens de ajuda nas popups**: mensagens de ajuda antes posicionadas nos diálogos antigos, serão exibidas em pop-up!

* **ES: Configurador de duração de popup**: você decide o tempo de duração da popup

* **ES: Nomes das linguagens na sua própria linguagem**: autoexplicativo ;)

* **ES: Reescrito o painel de configuração de controles**: a configuração de controles foi errr ... modernizada, veja a postagem dedicada ao assunto

* **ES: Atualizado processo de seleção de Núcleo / Emulador para evitar inception na interface e exibir valores padrão**: será mais fácil de ler e compreender + exibir valores padrão

* **ES: Atualizado tamanho de fonte na interface de metadados de jogos**: autoexplicativo

* **ES: Removido código morto da RomsManager**: removendo código inútil para reduzir o uso de memória

* **ES: Tornar a ordem da lista de jogos persistente**: A ordem da lista de jogos era perdida durante um reiniciar após a alteração de outras opções... agora ela é persistente

* **ES: Exibir conteúdo do diretório no painel de detalhes**: Quando um diretório era selecionado na lista de jogos, os dados do jogo não eram recarregados, mas agora é exibido um resumo dos jogos constantes em tal diretório (podendo configurar temas) 

* **ES: Exibir jogo em diretórios com apenas um jogo**: Diretórios contendo apenas um jogo (ex PSX, Dreamcast...) são diretamente exibidos como jogo e você não precisa mais entrar no diretório para visualizar e executar um jogo

* **ES: Adicionada opção para listar o conteúdo do diretório ao invés do seu nome**: Esta opção habilita a exibição de nomes de jogos dentro de estruturas complexas de diretórios numa lista única, escondendo os diretórios

* **ES: Exibir apenas letras disponíveis na opção Ir Para Letra + Exibir figuras**: A opção "Ir para letra" exibirá apenas letras e figuras disponíveis

* **ES: Evitar recarrega de imagens se origem não for alterada**: Otimização simples de visualização

* **ES: Atualizar lista de jogos no próprio menu da lista de jogos**: A opção "Atualizar lista de jogos" está disponível diretamente no sistema

* **ES: Configurar lista em modo loop**: Ir diretamente para o final da lista usando "cima" na primeira entrada, clássico, mas é uma pena não ter isto até agora!

* **ES: Corrigido botão voltar na lista de jogos**: O voltar dentro de subdiretórios retornava para o nível raiz, e agora vai para nível -1

* **ES: Corrigido botão ao sair do jogo**: Sair de um jogo levava ao nível raiz da lista de jogos, e agora permanece onde estava

* **ES: Corrigido detalhe de painel ausente após pular para a letra A**: Pular para o primeiro jogo não recarregava o painel de detalhes, agora está corrigido

* **ES: Corrigida adição de favorito na primeira vez (requeria dois toques)**: Dependendo da lista, adicionar um jogo aos favoritos exigia que ele fosse adicionado duas vezes seguidas

* **ES: Manter a seleção / ordem na lista de jogos no nível do subdiretório**: Ordenar uma lista de jogos alterava até a configuração do nível raiz da lista, e isto foi corrigido

* **Atualização do kodi-plugin-video-youtube para 5.5.1**: este plugin está evoluindo muito rápido mas sem um longo histórico de versões, por isto o atualizamos

* **recalbox-config.sh: adicionado getEmulatorDefaults**: um auxiliador para desenvolvedores que desejam saber qual o emulador padrão

* **Adicionadas fontes que suportam todas as linguagens**: agora sim! Qualquer linguagem do mundo inteiro deverá ser exibida corretamente.

* **picodrive: reescrita parcial do pacote + reabilitado para o odroidc2**: Usuários do C2 ficarão felizes, picodrive está de volta!

* **Adicionados métodos alternativos de detecção de placas de som**: não terá impacto ainda, mas será de utilidade para novas placas que não declaram seus dispositivos de som no padrão

* **Tempo de inicialização otimizado**: reescrito um timeout problemático, ES iniciará mais rápido!

* **Wifi: opção de alteração de região adicionada ao recalbox.conf - o wifi.region=FR corrige problemas com o canal 12 por exemplo**: usuários franceses sabem o que isto significa: não serão mais restritos aos canais de wifi dos EUA, podendo utilizar finalmente os canais 12 e 13 da WiFi interna do Pi3!

* **Antiga API removida, nova API em desenvolvimento**: a API antiga não é mantida há 2 anos e nem ao menos inicia desde o Recalbox 4.1, então foi eliminada

* **mais informação nos arquivos de suporte**: para nos ajudar a prestar um melhor suporte

* **som: correção para a perda de configuração após uma atualização**: este bug traiçoeiro ocorre no PC e após a atualização do Recalbox a configuração de áudio é perdida. Isto deverá ocorrer uma última vez com esta atualização, e não deverá se repetir daqui pra frente.

* **Adicionado aviso sobre desligamento do Recalbox**: algumas pessoas desligam seus recalbox muito cedo, gerando corrupção de dados. Esta mensagem deverá avisar estas pessoas para que não desliguem seus Recalbox cedo demais

* **Adicionado sistema Daphne com o emulador Hypseus**: curta jogos laserdisc!

* **pin356ONOFFRESET: melhorias de comportamento com o case nespi+**: configure o tipo de botão power (powerswitch) para pin356ONOFFRESET e curta seu case nespi+

* **Adicionada opção de duração para a tela de abertura no recalbox.conf**: você pode agora dizer ao Recalbox quanto tempo deve levar reproduzindo vídeos de abertura : como sempre, por um tempo definido, ou até o final do vídeo (o que atrasa o iniciar do ES)

* **Atualizado mGBA para rodar gb e gbc com suporte ao Super Game Boy**: o mGBA agora consegue executar roms do GameBoy e GameBoy Color no formato super gameboy. O super gameboy foi um periférico do SNES para compatibilizar roms de GB no seu SNES !

* **Adicionado suporte ao Thomson TO8(D) com o núcleo libretro theodore, graças a zlika**: Foi o primeiro computador que eu usei em minha vida e que me mostrou que eu trabalharia com TI quando crescesse! 

* **linapple: apple2.configfile=dummy evita que a configuração seja sobrescrita**: agora você pode dizer para o Recalbox não gerar arquivos de configuração linapple

* **Adicionado core Amstrad PCP: crocods**: não possui teclado virtual, mas funciona bem

* **Nova versão libretro-o2em suporta salvar e rebobinar**: autoexplicativo!

* **Atualizados todos o núcleos libretro**: para um melhor netplay :D

* **Atualizado Retroarch para 1.7.3 + alguns patches necessários**: obrigatório! Eu recomendo fortemente que você leia o [blog libretro - em inglês](https://www.libretro.com/index.php/retroarch-1-7-3-released/) para entender todos os detalhes. Algumas pessoas notarão que podem usar monitores 15kHz no PC e obter as configurações corretas ;) Infelizmente não tivemos tempo de testar, mas faremos assim que possível! Compartilhe suas experiências no fórum!

* **configgen: N64 novamente no modo tela cheia nativa, sem resolução forçada**: até o momento, tínhamos que simular o tamanho da janela em toda a tela para remediar bugs de áudio. Agora, voltaremos para o modo tela cheia nativa com o mupen64plus

* **configgen: a sintaxe de vídeo "auto DMT 4 HDMI" funciona para o N64:**: esta nova variação específica da opção `auto` se aplica aos monitores que suportam tal resolução (funcionalidade apenas para o pi) e também é suportada no N64 a partir de agora

Este é um changelog gigaaaaaaaaaante, e estamos muito orgulhosos do nosso trabalho com Netplay! Esperamos que vocês gostem deste lançamento e compartilhem sua experiência! Recomendamos fortemente que vocês ativem os retroachievements com o netplay já que são compatíveis!
