+++
date = "2017-12-03T11:00:00+02:00"
image = "/images/blog/2017-12-03-recalbox-17-12-02/recalbox-17.12.02-banner.jpg"
title = "Recalbox 17.12.02"

[author]
  github = "https://github.com/digitallumberjack"
  gitlab = "https://gitlab.com/digitallumberjack"
  twitter = "https://twitter.com/digitalumberjak"
  name = "digitalLumberjack"
[translator]
  github = "https://github.com/robsdedude"
  gitlab = "https://gitlab.com/robsdedude"
  name = "Robsdedude"
+++

Hi!

Letzten Freitag haben wir Version `17.12.01` veröffentlicht. Einige Leute haben Ruckeln beim Durchsuchen der Spiele oder gar den Spielen selbst bemerkt.

Das liegt am Steam-Controller-Service der beim Hochfahren automatisch gestartet wird selbst wenn ihr gar keinen Steam Controller habt.

Wir haben das Problem behoben und ihr könnt eure Recalbox jetzt auf `17.12.02` aktualisieren.

Für eine Neuinstallation findet ihr die Recalbox-Images auf [archive.recalbox.com](https://archive.recalbox.com)