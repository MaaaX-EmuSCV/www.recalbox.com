+++
date = "2018-03-16T01:00:00+02:00"
image = "/images/blog/2018-03-16-recalbox-18-03-16/recalbox-18.03.16-banner.jpg"
title = "Recalbox 18.03.16"

[author]
  github = "https://github.com/substring"
  gitlab = "https://gitlab.com/substring45"
  name = "Substring"

+++

Hi there fellow retrogamers!

I hope that all of you enjoyed our previous release and had fun playing some Amiga games again on your Pi!

Recalbox spring is coming a bit early this year with a few features you don't want to miss! Here we go for the changelog:

* ES: new carousel mode and new theming options! Check UI Settings > Theme configuration with recalbox-next theme

* ES: no more white screen of death when VRAM is filled! Themers will finally be able to add 1080p background images

* segacd: add chd as supported file format

* sound: only set volume for playback devices. This will help most x86 users who had some feedback loop problem on first boot.

* psx: fixed typo (mu3 instead of m3u file extension)

* Moonlight: bump to 2.4.6, supports up to GFE 3.12 + can handle several GFE PC + rewrote scrape. Yeah, finally you can update GFE on your PC! And scraping data is now a mix of game infos on TGDB + GFE cover image. Which means, you can set the game cover image in GFE

* recalbox.conf videomode now supports a new `auto` mode which will help people with special screens. How it works: Recalbox will detect if the screen can handle 720p. If it does, switch to 720p. If not, stay on the default resolution. This is not set by default for now.
 
* Pi: Disabled sound forced to HDMI, will now auto detect. This combines great with the auto mode previously detailled

* ScummVM: Enable MT-32 emulation for better music for the PC version of games

* DosBox: bump to r4076 + add a virtual keyboard where you can map some joystick buttons on the keyboard and play any DOS game with a pad! Check [here](https://gitlab.com/recalbox/recalbox/issues/363) for some info. For lazy people: hit CTRL+F1

* Bump FBA-LIBRETRO for bugfixes
 
* Solved a small bug for MAME roms default naming

* Bumped PPSSPP to v1.5.4

* Restored the original splash video + added a few new ones we hope you'll enjoy ;) They are randomly selected at boot

* Fixed a small typo for x68000 multidisk using .m3u files

* You can launch some games from the web manager

* Added the MUSIC keyword for network shares in `/boot/recalbox-boot.conf`

* ES: music subfolders are scanned, fixed a few bugs on startup, only start Kodi on systems view, Wifi SSID selector, fixed occasionnal black screen when coming back from emulators

Have fun with this new release, and share your feedback on the forum!
