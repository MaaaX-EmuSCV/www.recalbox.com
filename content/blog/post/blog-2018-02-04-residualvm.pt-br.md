+++
date = "2018-02-04T07:00:00+00:00"
image = "/images/blog/2018-02-04-residualvm/recalbox-residualvm-banner.png"
title = "ResidualVM"

[author]
  github = "https://github.com/rockaddicted"
  gitlab = "https://gitlab.com/rockaddicted"
  name = "rockaddicted"
[translator]
  github = "https://github.com/nwildner"
  gitlab = "https://gitlab.com/nwildner"
  name = "nwildner"
+++

Olá amigos,

Hoje um novo sistema emulado está disponível no Recalbox: **ResidualVM !**

Este é um projeto irmão do **ScummVM**, que permite que você execute jogos que usam o motor de jogo **GrimE**. Três jogos são compatíveis com este emulador:
**Grim Fandango**, **Escape from Monkey Island** e **Myst III - Exile**.  

Este novo emulador foi adicionado pelo [lmerckx](https://gitlab.com/lmerckx) como um **core ScummVM secundário**. A permuta entre ScummVM e ResidualVM é automática. Até mesmo a forma de adicionar novos jogos é a mesma do ScummVM. A única diferença é a extensão utilizada. Uma vez adicionados os jogos no diretório **/recalbox/roms/scummvm**, você deve nomear arquivo de atalho como **nomeCurtoDoJogo.residualvm** o invés de **nomeCurtoDoJogo.scummvm** (por exemplo **grim.residualvm** para adicionar o Grim Fandango).
O Recalbox irá selecionar automaticamente o emulador.

Caso você queira saber mais informações sobre este novo emulador, leia a [documentação do ResidualVM](http://www.residualvm.org/documentation/).
E a **lista de compatibilidade** pode ser encontrada [aqui](http://www.residualvm.org/compatibility/)


Esperamos que vocês curtam este novo emulador e (re)descubram a biblioteca de jogos do **ResidualVM / GrimE**

## Grim Fandango
[![grim01](/images/blog/2018-02-04-residualvm/grim01-thumb.jpg)](/images/blog/2018-02-04-residualvm/grim01.jpg)
[![grim02](/images/blog/2018-02-04-residualvm/grim02-thumb.jpg)](/images/blog/2018-02-04-residualvm/grim02.jpg)
[![grim03](/images/blog/2018-02-04-residualvm/grim03-thumb.jpg)](/images/blog/2018-02-04-residualvm/grim03.jpg)

## Escape from Monkey Island
[![monkey01](/images/blog/2018-02-04-residualvm/monkey01-thumb.jpg)](/images/blog/2018-02-04-residualvm/monkey01.jpg)
[![monkey02](/images/blog/2018-02-04-residualvm/monkey02-thumb.jpg)](/images/blog/2018-02-04-residualvm/monkey02.jpg)
[![monkey03](/images/blog/2018-02-04-residualvm/monkey03-thumb.jpg)](/images/blog/2018-02-04-residualvm/monkey03.jpg)

## Myst III - Exile
[![myst01](/images/blog/2018-02-04-residualvm/myst01-thumb.jpg)](/images/blog/2018-02-04-residualvm/myst01.jpg)
[![myst02](/images/blog/2018-02-04-residualvm/myst02-thumb.jpg)](/images/blog/2018-02-04-residualvm/myst02.jpg)
[![myst03](/images/blog/2018-02-04-residualvm/myst03-thumb.jpg)](/images/blog/2018-02-04-residualvm/myst03.jpg)

