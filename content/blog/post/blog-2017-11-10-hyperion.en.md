+++
date = "2017-11-10T20:00:00+00:00"
image = "/images/blog/2017-11-10-hyperion/recalbox-hyperion-banner.jpg"
title = "Make your recalbox shine with Hyperion!"

[author]
  github = "https://github.com/digitallumberjack"
  gitlab = "https://gitlab.com/digitallumberjack"
  name = "digitalLumberjack"
  twitter = "https://twitter.com/digitalumberjak"

+++

**Hyperion** (https://github.com/hyperion-project/hyperion) is a free and open source project to turn your RaspberryPi in a **Ambilight-like** system!

With Hyperion and recalbox, you will turn your retrogaming and multimedia system into an immersive and beautiful experience!


## I - Description

First, we are not talking about using Hyperion with a subset of your recalbox like **Kodi**.
You will be able to use Hyperion features in **EVERY SINGLE RETRO GAME** you play on recalbox!

![Hyperion with recalbox](/images/blog/2017-11-10-hyperion/pukerainbow.gif)

RGB leds will **dynamically extend** your TV image (Flat or CRT) on the walls.

What does it look like? See with _Sonic 3_ on Sega Megadrive/Genesis:

[![Sonic Title](/images/blog/2017-11-10-hyperion/sonic1-thumb.jpg)](/images/blog/2017-11-10-hyperion/sonic1.jpg)
[![Sonic Plane](/images/blog/2017-11-10-hyperion/sonic2-thumb.jpg)](/images/blog/2017-11-10-hyperion/sonic2.jpg)
[![Sonic Stage 1](/images/blog/2017-11-10-hyperion/sonic3-thumb.jpg)](/images/blog/2017-11-10-hyperion/sonic3.jpg)

And in video:

<iframe width="560" height="315" src="https://www.youtube.com/embed/z1QblkdO6bs" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## II - Hardware

Hyperion supports many LED strips (see https://hyperion-project.org/wiki/Supported-hardware) with various protocols:

* **SPI**: LPD6803, LPD8806, APA102, WS2812b, and WS2801
* **USB**: AdaLight and SEDU
* **PWM**: WS2812b

Tests were made on **WS2812b** with PWM and SPI. I never managed to make PWM work correctly, so we will describe how to make your recalbox/hyperion installation with the **WS2812b in SPI** mode. This section is missing from the Hyperion wiki, so we will explain how to use that here (and update the Hyperion wiki with those informations).

The support of **WS2812b** with one wire over SPI has been added by [penfold42](https://github.com/penfold42) with [this commit](https://github.com/hyperion-project/hyperion/commit/a960894d140405e29edb413685e700d2f1714212). Thank you.

The following tutorial makes use of soldering iron. If you already have one, the rest of the hardware is kind of cheap. But if you want to make something without soldering, see the official tutorial here :
https://hyperion-project.org/threads/raspberry-pi-3-mediacenter-hyperion-ambilight-no-soldering.77/


You will need:

* your recalbox
* a WS2812b led strip [<i class="fa fa-picture-o"></i>](/images/blog/2017-11-10-hyperion/ws2812b.jpg)
* a 330 Ohms resistor to set on the data pin
* something to cut the strip [<i class="fa fa-picture-o"></i>](/images/blog/2017-11-10-hyperion/pince.jpg)
* a soldering iron
* wires or connectors to connect led strips [<i class="fa fa-picture-o"></i>](/images/blog/2017-11-10-hyperion/connectors-item.jpg)
* a voltage converter to shift the data voltage to 5V [<i class="fa fa-picture-o"></i>](/images/blog/2017-11-10-hyperion/voltage.jpg), or a level shifter [<i class="fa fa-picture-o"></i>](/images/blog/2017-11-10-hyperion/shifter.jpg) to reduce the VCC of the leds.
* some dupont jumpers to easily connect wires to your RaspberryPi [<i class="fa fa-picture-o"></i>](/images/blog/2017-11-10-hyperion/dupont.jpg)
* a 4amp (min) 5v power supply [<i class="fa fa-picture-o"></i>](/images/blog/2017-11-10-hyperion/powersupply.jpg)

Don't hesitate to [visit the <img style="vertical-align: baseline;" src="/images/shared/recalstore.png"/>](https://www.recalbox.com/shop) to find all you need.


Of course if you want to save money you can :

* solder directly everything (no need for connectors and dupont)
* Use a 3amp power supply. I tested my 220 led hyperion LED with an Aukru 5V 3AMP, and it works almost well, but when all leds are white, the end of the strip is a bit yellow...


## III - Wiring

#### Leds

Measure your TV borders and cut 1 strip by tv border according to its size.

Solder your leds strip together OR solder connectors on strips. Follow arrows on the strip to known what connector you should use (IN or OUT).

How it looks like:

[![connectors](/images/blog/2017-11-10-hyperion/connectors-thumb.jpg)](/images/blog/2017-11-10-hyperion/connectors.jpg)
[![connectors](/images/blog/2017-11-10-hyperion/solder-thumb.jpg)](/images/blog/2017-11-10-hyperion/solder.jpg)



#### The system

To allow the RPi to send data to the LED, you have the choice:

* lower the LED VCC to 4.7V with a voltage regulator
* shift the GPIO signal to 5V with a level shifter

###### A - Voltage Regulator

[![Circuit voltage regulator](/images/blog/2017-11-10-hyperion/wiring-regulator.png)](/images/blog/2017-11-10-hyperion/wiring-regulator.png)

(original image from [hyperion wiki](https://hyperion-project.org/wiki/3-Wire-PWM))

If you are on RPI2 or 3, you have more GPIO but the pinout is still the same.

The RPi is connected to it's own power supply, and to the ground of the LED power supply.

The RPi streams data through the SPI MOSI GPIO to the data wire of the LED.

The voltage regulator is connected between the LED power supply and the LED.

###### B - Level shifter for gpio (untested)


If you chose to regulate GPIO voltage, use the schema above, just remove the voltage regulator and connect the level shifter between the GPIO MOSI and the LED data wire.


## IV - Configuration

#### Hypercon

**Hypercon** is a graphic user interface that let you configure your Hyperion installation, and create a Hyperion configuration.

Download and start **Hypercon** following the official tutorial: https://hyperion-project.org/wiki/HyperCon-Information

[![hypercon screenshot](/images/blog/2017-11-10-hyperion/hypercon-thumb.jpg)](/images/blog/2017-11-10-hyperion/hypercon.jpg)


You will find many resources about the Hypercon configuration on the Hyperion wiki: https://hyperion-project.org/wiki/HyperCon-Guide

Here is how to configure Hypercon for your recalbox and the **WS2812b in spi mode**:

* **Hardware**
 * Type -> WS281X-SPI
 * RGB Byte Order -> GRB
 * Construction and Image Process -> configure depending of your installation
 * Blackborder Detection -> Enabled
     * Threshold -> 5
     * mode -> osd
* **Process**
  * _Smoothing_
     * Enabled -> True
     * Time -> 200ms
     * Update Freq -> 20
     * Update Delay -> 0
* **Grabber**
  * _Internal Frame Grabber_
     * Enabled -> True
     * Width -> 64
     * Height -> 64
     * Interval -> 200
     * Priority Channel -> 890
  * _GrabberV4L2_
     * Enabled -> False
* External
  * Booteffect / Static Color -> configure the boot effect you like here!


Then click on the **Create Hyperion Configuration** button and save the json file as `hyperion.config.json`.

Copy this file on your recalbox (by ssh, or via your computer shared networks) in `/recalbox/share/system/config/hyperion/hyperion.config.json` (`config/hyperion/hyperion.config.json` on the network)


#### Enable hyperion service in recalbox.conf

Just enable Hyperion in [recalbox.conf](https://github.com/recalbox/recalbox-os/wiki/recalbox.conf-%28EN%29):  
Change the line with `hyperion.enabled=0` to `hyperion.enabled=1` (remove the `;`)

Edit the config.txt file located on the boot partition and add `dtparam=spi=on`:

```bash
mount -o remount,rw /boot && echo 'dtparam=spi=on' >> /boot/config.txt
```

And reboot.


## V - Enjoying the rainbow

You're done! You have a fully functional Ambilight-like system! Test some retro games, movies and animes with Kodi, and let the beauty of colors enhance your multimedia experience!

![](/images/blog/2017-11-10-hyperion/nyan.jpg)
