+++
date = "2017-11-17T10:30:00+02:00"
image = "/images/blog/2017-11-17-bump-emulators/recalbox-bump-emulators-banner.png"
title = "Emulators Update"

[author]
  github = "https://github.com/rockaddicted"
  gitlab = "https://gitlab.com/rockaddicted"
  name = "rockaddicted"
[translator]
  github = "https://github.com/robsdedude"
  gitlab = "https://gitlab.com/robsdedude"
  name = "Robsdedude"
+++

Liebe Recalboxer,

mit dieser neuen Version wird es Zeit, die Emulatoren von Recalbox zu aktualisieren.

Schauen wir uns die größten Bugfixes und neuen Features im Detail an:
   
   
- **RetroArch** : 

  - Auf **1.6.9** aktualisiert. Das bedeutet viele Fixes und Verbesserungen. Mehr Informationen findet ihr im [libretro release Post](https://www.libretro.com/index.php/retroarch-1-6-9-released/) (englisch).

- **ScummVM** :

  - Auf Version **1.10.0** "WIP" aktualisiert. Diese Version funktioniert auf allen Architekturen von Recalbox, inklusive Odroid XU4. Viele Fehler die eine ruckelige Maussteuerung mit dem Analog-Stick verursacht haben, wurden behoben. Mehr Infos findet ihr im 1.10.0 ScummVM [changelog](https://github.com/scummvm/scummvm/blob/master/NEWS#L4) (englisch).

- **DosBox** :

  - Auf **0.74 r4063** aktualisiert. Danke, [lmerckx](https://gitlab.com/lmerckx)

- **Libretro cores** :

  - **libretro-81**
  - **libretro-beetle-lynx**
  - **libretro-beetle-ngp**
  - **libretro-beetle-pce**: MAME CHD Images werden unterstützt
  - **libretro-beetle-supergrafx**: Unterstützt RetroAchievements, Turbo an/aus im 2-Button-Controller- Modus und MAME CHD Images
  - **libretro-beetle-vb**
  - **libretro-beetle-wswan**: Verdrehtes Bild behoben
  - **libretro-bluemsx**: Unterstützt CAS und M3U Dateien
  - **libretro-cap32**
  - **libretro-catsfc**: Tonprobleme behoben, CPU-Emulation, Lag-Fix, BSX Fixes und SuperFX Fixes und Verbesserungen
  - **libretro-fba**: Auf die neuste Version v0.2.97.42 aktualisiert. Neue DAT Dateien gibt es [hier](https://gitlab.com/recalbox/recalbox/tree/master/package/recalbox-romfs/recalbox-romfs-fba_libretro/roms/fba_libretro/clrmamepro)
  - **libretro-fceumm**: Viele Fixes und Neuerungen im Mapper-Code
  - **libretro-fmsx**: .dsk/.cas Dateien werden unterstützt, nospritelimit & crop overscan Optionen für den Core, einige Multi-Disk Probleme behoben
  - **libretro-fuse**
  - **libretro-gambatte**: Farbpaletten-Bugs behoben
  - **libretro-genesisplusgx**: Neuer, cycle-genauer Sound-Core, Übertaktung möglich, Sprites-Pro-Zeile-Limits können deaktiviert werden, Probleme bei der CD-Hardware-Buffer-Initialisierung mit dynamischer Speicherallokierung behoben
  - **libretro-glupen64**: Höhere Auflösung
  - **libretro-gpsp**
  - **libretro-gw**: Nicht korrekt aktualisierende Highscores behoben
  - **libretro-hatari**
  - **libretro-imame**: Viele Fixes und Verbesserungen
  - **libretro-lutro**: Viele Fixes und Verbesserungen
  - **libretro-mame2003**: DCS Ton-Probleme behoben (Mortal Kombat 1/2/3/Ultimate, NBA Jam, Total Carnage u.a. Spiele), Verbesserte Spielekompatibilität, rückportierter C basierter MIPS3-Support (Killer Instinct und Killer Instinct 2 laufen nur auf X86/X86_64)
  - **libretro-meteor**
  - **libretro-mgba**: Auf Upstream 0.6.1 Release aktualisiert, viele Verbesserungen
  - **libretro-nestopia**: Unterstützt 2-fach Übertaktung
  - **libretro-nxengine**: Auf v1.0.0.6 aktualisiert
  - **libretro-o2em**
  - **libretro-pcsx**: Polygon-Probleme behoben, Möglichkeit zum aktivieren/deaktivieren der Dithering-Option hinzugefügt
  - **plibretro-picodrive**: Verbesserungen der Genauigkeit, 68k-Übertakten möglich
  - **libretro-pocketsnes**
  - **libretro-prboom**: Viele Cursor bezogene Fixes, Gamepads, Savegames, Maus/Keyboard-Unterstützung
  - **libretro-prosystem**: Probleme mit Datenbank und Farbpalette behoben
  - **libretro-quicknes**
  - **libretro-snes9x-next**: Ungültigen VRAM-Zugriff beseitigt, Speedhack für SuperFX
  - **libretro-snes9x**: MSU1-Fixes (Knacken im Ton), Fixes für chrono trigger, Seitenverhältnis korrigiert und 20MHz-SuperFX Übertaktungsoption
  - **libretro-stella**
  - **libretro-tgbdual**
  - **libretro-vecx**: Line drawing aktualisiert und Core-Option für internen Auflösungsmultiplikator hinzugefügt
  - **libretro-vice**: Verbesserte Bildskalierung und -handhabung für PAL/NTSC Regionen

- **libretro-mame2003 - Beispiele für neu unterstützte Spiele** :

[![cute_fighter](/images/blog/2017-11-17-bump-emulators/cutefght-thumb.png)](/images/blog/2017-11-17-bump-emulators/cutefght.png)
[![dj_boy](/images/blog/2017-11-17-bump-emulators/djboy-thumb.png)](/images/blog/2017-11-17-bump-emulators/djboy.png)
[![dreamworld](/images/blog/2017-11-17-bump-emulators/dreamwld-thumb.png)](/images/blog/2017-11-17-bump-emulators/dreamwld.png)
[![vasara2](/images/blog/2017-11-17-bump-emulators/vasara2-thumb.png)](/images/blog/2017-11-17-bump-emulators/vasara2.png)
   
[![gaia_the_last_choice_of_zarth](/images/blog/2017-11-17-bump-emulators/gaialast-thumb.png)](/images/blog/2017-11-17-bump-emulators/gaialast.png)
[![gogo_mile_smile](/images/blog/2017-11-17-bump-emulators/gogomile-thumb.png)](/images/blog/2017-11-17-bump-emulators/gogomile.png)
[![rolling_crush](/images/blog/2017-11-17-bump-emulators/rolcrush-thumb.png)](/images/blog/2017-11-17-bump-emulators/rolcrush.png)
[![regulus](/images/blog/2017-11-17-bump-emulators/regulus-thumb.png)](/images/blog/2017-11-17-bump-emulators/regulus.png)
   

Mit dem neuen Release aktualisieren wir also fast alle Emulatoren von Recalbox.
Die fehlenden (advancemame, dolphin, moonlight, mupen64plus, ppsspp) werden später in einem nächsten Update aktualisiert.

Wir hoffen es bereitet euch Freude. Wir bedanken uns beim [libretro-Team](https://github.com/orgs/libretro/people), dem [ScummVM-Team](https://github.com/orgs/scummvm/people) und allen Leuten, die an den Emulatoren arbeiten.

Viele Grüße
das Recalbox-Team