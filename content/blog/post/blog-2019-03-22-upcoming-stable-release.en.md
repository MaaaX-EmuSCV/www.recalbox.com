+++
date = "2019-03-22T11:54:12Z"
image = "/images/blog/2019-03-22-upcoming-stable-release/banner.jpg"
title = "Upcoming stable release"

[author]
  name = "Fab2Ris"
  facebook = "https://www.facebook.com/fabriceboutard"
  twitter = "https://twitter.com/Fab2Ris"

+++

Good news!

We are only a few days away from the next Recalbox stable release!

But first, we need your help!
Help us hunt down any remaining critical bug that we may have missed, before the final release.

## Versioning scheme

As you probably noticed, a few versions ago, we changed our versioning scheme: we were previously using numbers (`4.0`, `4.1`, …) but switched to time-based version numbers (`18.04.20`, `18.06.27`, …) rather than bumping into the `5.x` branch.

Unfortunately, time-based version numbers are difficult to read, difficult to write, difficult to pronounce. In a word: they are not friendly.

Thus, we decided to **switch back to numbered versions** which are easier to use, more semantic (you know how important a version is based on its number, its release date actually provides very few useful information).

Anyway, we consider all those date-based version to be the `5.x` branch and the next stable release will thus be labeled… `6.0` 🎉

As for our kids, we will probably assign a name to the releases we prefer 😉 Nothing has been decided yet for the very next one, but it will definitively receive a name! ❤️

## Release Candidate 1

Our development team has spent the last few months piling up so many great features, emulators, updates, themes, systems, hardware support, … We won't dig into the details of those exciting new stuff in this post but expect a thorough description of the main ones in the upcoming final release blog post.

We shipped such an amount of new code that we had to test every optimisation at great length the last few months to prevent any issue. It's one of the main reasons this release took so long: we tested it extensively to make your gaming experience as smooth as possible.

And this was made possible thank to all of you!

With the help of our awesome community (assisted by our just-as-awesome support team!), we were able to release at least 3 different _beta_ versions that raised dozens of new bugs that our developers were able to fix as quickly as they could (remember how everyone is volunteering, in our team?).

Anyway, we have come a long way to release this new Recalbox version!

But today, we are super confident and super proud of our work to the extent that we could release it as-is, right now.

But that's not enough! (yet) We want this next stable release to be what you expect it to be: stable 💪

That's why **we are releasing today a Release Candidate (RC)**. As quite explicitly stated by its name, a Release Candidate is a version of the software that is a good candidate to be the final release. We are publishing it a few days prior to the final release, so that people can test it on an even wider range of hardware, setups, _bioses_ and _roms_ in order to catch any critical-level bug that we may have missed 😬

As you may know, _beta_ versions target advanced users who know how to find, reproduce and properly report a bug (providing log files excerpts). A Release Candidate, on the other hand, is supposed to be usable my mid-level users who can just properly reproduce and report a bug (and that's already quite a big deal!). It shall not be used if you don't know how to flash a SD-card, test some games or configure a controller, though.

**What we expect from you** if you want to help, is to [download the image](https://recalbox-releases.s3.nl-ams.scw.cloud/stable/index.html) available for your hardware, configure it to your taste (options, games, themes, …) or even use your previous configuration and let us know if anything breaks 💥

If you encounter some critical issue, please reach out to us on [Discord](https://discord.gg/d2xCQ4e) with as many details as possible for us to understand and fix the issue efficiently:

* Recalbox version (there might be more than one RC)
* your hardware: board and controllers
* the theme you are using
* potential changes compared to default configuration
* detailed steps to reproduce the bug
* expected behaviour
* observed behaviour

There are a few things that are out-of-scope for this RC and that should not be particularly reported (they should be reported but will probably not be fixed before the stable release):

* specific controller issues
* specific _rom_ issues
* specific theme issues
* known bugs/issues (that will be fixed after final release):
  * cheats not working with Mame2010
  * Retroarch screen ratio unsynchronised with ES/Configgen
  * Kodi is not starting with some pads

## Upgrades (no!)

If you are already running Recalbox latest stable version or one of our latest _beta_ versions and would like to upgrade to this RC using the in-app update system: **don't!**

We revamped Recalbox so deeply that the upgrade system and infrastructure are now incompatible. There is unfortunately no way to update from a previous version to this one.

Updates from this RC to a future RC or stable release should work, though.

Please **perform a fresh install**, test, play, hack, have fun… and let us know how it goes 🤗
