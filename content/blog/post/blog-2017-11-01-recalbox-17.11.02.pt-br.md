+++
date = "2017-11-02T20:00:00+02:00"
image = "/images/blog/2017-11-01-recalbox-17.11.02/title-17.11.02.png"
title = "Recalbox 17.11.02"

[author]
  github = "https://github.com/digitallumberjack"
  gitlab = "https://gitlab.com/digitallumberjack"
  name = "digitalLumberjack"
  twitter = "https://twitter.com/digitalumberjak"

[translator]
  github = "https://github.com/nwildner"
  gitlab = "https://gitlab.com/nwildner"
  name = "nwildner"
+++

Olá a todos!

Mesmo com o Recalbox **4.1** não completando 3 semanas de vida, vocês foram mais de 100.000 retrogamers a instalar ou atualizar o Recalbox, e vários de vocês nos deram feedback !

Prometemos que lançaríamos novas versões muito mais rápido que uma ou duas vezes ao ano, e é hora de provar ! Cabe a você verificar o nosso esforço que fez com que a versão **4.1** fosse lançada tão tarde !

Você deve ter notado que abandonamos o sistema de numeração antigo _4.1_ em prol de um novo sistema de revisão "semântica" baseado na data de lançamento. Então, com esta nova versão **17.11.02** fica muito mais fácil gerenciar !


Vamos parar de papo furado e dar uma olhada nas evoluções:

* Novas firmwares para x86 (na maioria dispositivos INTEL)
* Moonlight executa sem problemas ao iniciar
* Melhorias nos logs de pareamento bluetooth
* Um pequeno aumento no tempo de escaneamento do bluetooth
* Corrigido bug no bluetooth que aplicava correções exclusivas do 8bitdo a qualquer dispositivo pareado
* A opção de segurança pode ser desabilitada corretamente
* Pad reordering agora funciona com o PPSSPP
* Suporte ao controle Moga pro adicionado
* Opções padrão do BlueMSX alteradas (MSX2/60Hz/ym2413=enabled)


Ainda há alguns bugs acontecendo que iremos corrigir mais tarde:

* Advancemame apenas toca som. Isso acontece com pessoas que atualizaram da 4.0.x, e você pode encontrar a correção [aqui](https://forum.recalbox.com/topic/9522/plantage-advance-mame) 
* Parear por bluetooth pode ser uma tarefa árdua ... Investigar e corrigir é mais complicado que parece e tomará algum tempo, e poderemos precisar do feedback de testadores.


Mantenha-nos informados sobre suas impressões no forum. Devo lembrar que nós criamos o novo [testers' corner](https://forum.recalbox.com/category/54/testers-s-corner) lá. Preste atenção nas regras desta seção.


Feliz update !
