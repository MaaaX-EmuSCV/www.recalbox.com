+++
date = "2018-01-28T11:00:00+00:00"
image = "/images/blog/2018-01-28-sharp-x68000/recalbox-sharp-x68000-banner.png"
title = "SHARP X68000"

[author]
  github = "https://github.com/rockaddicted"
  gitlab = "https://gitlab.com/rockaddicted"
  name = "rockaddicted"
[translator]
  github = "https://github.com/robsdedude"
  gitlab = "https://gitlab.com/robsdedude"
  name = "Robsdedude"
+++

Hallo Leute,

es ist an der Zeit ein neues System für Recalbox zu begrüßen: das **SHARP X68000**!  


Als Emulator für das System nutzen wir den [libretro px68k Core](https://github.com/libretro/px68k-libretro), der auf dem [px68k Emulator](https://github.com/hissorii/px68k) von Hissorii basiert.  


Beachtet, dass ein neues BIOS für den Core benötigt wird. Hier findet ihr die passenden [Dateien für eure Recalbox](https://github.com/recalbox/recalbox-os/wiki/F%C3%BCge-ein-System-BIOS-hinzu-%28DE%29):  

- **7fd4caabac1d9169e289f0f7bbf71d8e** - **keropi/iplrom.dat**
- **cb0a5cfcf7247a7eab74bb2716260269** - **keropi/cgrom.dat**


Die Dateien müssen in ein **Unterverzeichnis** namens "keropi".  


Für weitere Infos zum neuen Core, könnt ihr euch die [libretro Dokumentation](https://buildbot.libretro.com/.docs/library/px68k) (englisch) anschauen.  


Wir hoffen, ihr habt Spaß mit dem neuen Core und könnt damit die SHARP X68000 Spielesammlung (wieder)entdecken.  


## Ein Video-Review der 30 besten SHARP X68000 Spiele 

<iframe width="560" height="315" src="https://www.youtube.com/embed/zmIXovryWrw" frameborder="0" allowfullscreen></iframe>
