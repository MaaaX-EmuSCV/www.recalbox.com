+++
date = "2018-01-30T11:00:00+02:00"
image = "/images/blog/2018-01-30-panasonic-3do/recalbox-panasonic-3do-banner.png"
title = "Panasonic 3DO"

[author]
  github = "https://github.com/OyyoDams"
  gitlab = "https://gitlab.com/OyyoDams"
  name = "OyyoDams"
+++

Salut les amis!

Nous sommes heureux de vous présenter une nouvelle console méconnue et pourtant aux performances remarquables à sa sortie: la **Panasonic 3DO!**

Malheureusement, ce nouveau système ne sera disponible que pour les versions Odroid XU4 et PC X86/X86_64 de Recalbox. En effet, sur Raspberry PI, même le 3, nous n'obtenons pas une émulation satisfaisante et cela même en utilisant les réglages les plus bas + l'activation d'un frameskip. Nous avons donc décidé, dans l'état actuel des choses, de ne pas activer ce système dans les autres versions de Recalbox.

Afin d'émuler ce système nous utiliserons le [core libretro 4do](https://github.com/libretro/4do-libretro) basé sur l'émulateur [4DO](http://www.freedo.org/).

Pour votre information, ce core nécessite l'ajout d'un nouveau bios. Voici le fichier [à ajouter dans votre Recalbox](https://github.com/recalbox/recalbox-os/wiki/Ajoutez-des-bios-%28FR%29) :

- **51f2f43ae2f3508a14d9f56597e2d3ce** - **panafz10.bin**

Nous espérons que vous allez apprécier ce nouveau core et que vous allez (re)découvrir la ludothèque de la Panasonic 3DO.

## Vidéo de présentation des 30 meilleurs jeux Panasonic 3DO

<iframe width="560" height="315" src="https://www.youtube.com/embed/un6u1nmvyTo" frameborder="0" allowfullscreen></iframe>
