+++
date = "2017-02-24T16:30:39+02:00"
image = "/images/blog/2017-02-24-scraper/title-scraper.jpg"
title = "ScreenScraper désormais supporté dans Recalbox!"
draft = false

[author]
  github = "https://github.com/rockaddicted"
  gitlab = "https://gitlab.com/rockaddicted"
  name = "rockaddicted"
  
+++

<p dir="auto">La toute dernière version de Recalbox vous propose maintenant un nouveau scraper intégré, basé sur <a href="http://screenscraper.fr/" rel="nofollow noreferrer" target="_blank">screenscraper</a>.</p><p dir="auto">Ce scraper vous permet de scraper vos jeux dans votre langue (EN, FR, DE, ES, PT) quand les informations sont disponibles dans la base de données.</p><p dir="auto">Il vous permet aussi de choisir entre des images normales ou composées: </p><p dir="auto"></p><div class="image-container"><a class="no-attachment-icon" href="https://www.recalbox.com/assets/images/blog/2017-02-24-scraper/es_scraper.jpg" target="_blank"><img src="/images/blog/2017-02-24-scraper/es_scraper.jpg" alt="es_scraper"></a></div><p dir="auto">Voici une petite vidéo de démonstration:</p><iframe width="560" height="315" src="https://www.youtube.com/embed/xd1i1mJdkjU" frameborder="0" allowfullscreen=""></iframe>