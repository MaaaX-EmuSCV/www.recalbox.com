+++
date = "2018-02-04T07:00:00+00:00"
image = "/images/blog/2018-02-04-residualvm/recalbox-residualvm-banner.png"
title = "ResidualVM"

[author]
  github = "https://github.com/rockaddicted"
  gitlab = "https://gitlab.com/rockaddicted"
  name = "rockaddicted"
[translator]
  github = "https://github.com/robsdedude"
  gitlab = "https://gitlab.com/robsdedude"
  name = "Robsdedude"
+++

Hallo ihr Lieben,  

ab heute gibt es ein neues System für Recalbox: **ResidualVM**  

Es handelt sich um das Schwester-Projekt von **ScummVM**, das es euch ermöglicht Spiele mit der **GrimE** Engine zu spielen. Drei Spiele sind mit dem Emulator kompatibel:  
**Grim Fandango**, **Escape from Monkey Island** und **Myst III - Exile**.  

Der neue Emulator wurde von [lmerckx](https://gitlab.com/lmerckx) als **zweiter ScummVM Core** hinzugefügt. Der Wechsel zwischen ScummVM und ResidualVM geschieht automatisch. Tatsächlich werden die neuen Spiele genauso hinzugefügt wie bei ScummVM. Lediglich die Datei-Endung ist anders. Wenn das Spiel in den Ordner **/recalbox/roms/scummvm** kopiert ist, muss sie nur noch in **gameShortName.residualvm** anstatt **gameShortName.scummvm** umbenannt werden (z.B. **grim.residualvm**, um Grim Fandango hinzuzufügen).  
Recalbox wird dann automatisch den richtigen Emulator starten.  

Für mehr Infos zum neuen Emulator könnt ihr in der [ResidualVM documentation](http://www.residualvm.org/documentation/) (englisch) nachsehen.  [Dort](http://www.residualvm.org/compatibility/) findet ihr auch eine **Kompatibilitätsliste**.  

Wir hoffen ihr habt Spaß am neuen Emulator und könnt die 
**ResidualVM/GrimE** Spielebibliothek (wieder)entdecken.  

## Grim Fandango
[![grim01](/images/blog/2018-02-04-residualvm/grim01-thumb.jpg)](/images/blog/2018-02-04-residualvm/grim01.jpg)
[![grim02](/images/blog/2018-02-04-residualvm/grim02-thumb.jpg)](/images/blog/2018-02-04-residualvm/grim02.jpg)
[![grim03](/images/blog/2018-02-04-residualvm/grim03-thumb.jpg)](/images/blog/2018-02-04-residualvm/grim03.jpg)

## Escape from Monkey Island
[![monkey01](/images/blog/2018-02-04-residualvm/monkey01-thumb.jpg)](/images/blog/2018-02-04-residualvm/monkey01.jpg)
[![monkey02](/images/blog/2018-02-04-residualvm/monkey02-thumb.jpg)](/images/blog/2018-02-04-residualvm/monkey02.jpg)
[![monkey03](/images/blog/2018-02-04-residualvm/monkey03-thumb.jpg)](/images/blog/2018-02-04-residualvm/monkey03.jpg)

## Myst III - Exile
[![myst01](/images/blog/2018-02-04-residualvm/myst01-thumb.jpg)](/images/blog/2018-02-04-residualvm/myst01.jpg)
[![myst02](/images/blog/2018-02-04-residualvm/myst02-thumb.jpg)](/images/blog/2018-02-04-residualvm/myst02.jpg)
[![myst03](/images/blog/2018-02-04-residualvm/myst03-thumb.jpg)](/images/blog/2018-02-04-residualvm/myst03.jpg)