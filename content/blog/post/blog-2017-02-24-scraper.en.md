+++
date = "2017-02-24T16:30:39+02:00"
image = "/images/blog/2017-02-24-scraper/title-scraper.jpg"
title = "ScreenScraper support on recalbox!"
draft = false

[author]
  github = "https://github.com/rockaddicted"
  gitlab = "https://gitlab.com/rockaddicted"
  name = "rockaddicted"
+++

<p dir="auto">The new recalbox release introduce a new embed scraper, base on <a href="http://screenscraper.fr/" rel="nofollow noreferrer" target="_blank">screenscraper</a> database.</p><p dir="auto">This scraper allows you to automatically scrape in your native language (EN, FR, DE, ES, PT) when informations are available in the database.</p><p dir="auto">It allows you too, to switch between normal or composed visuals: </p><p dir="auto"></p><div class="image-container"><a class="no-attachment-icon" href="https://www.recalbox.com/assets//images/blog/2017-02-24-scraper/es_scraper.jpg" target="_blank"><img src="/images/blog/2017-02-24-scraper/es_scraper.jpg" alt="es_scraper"></a></div><p dir="auto">Here is a short demo video:</p><iframe width="560" height="315" src="https://www.youtube.com/embed/xd1i1mJdkjU" frameborder="0" allowfullscreen=""></iframe>