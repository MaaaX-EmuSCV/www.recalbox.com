+++
date = "2018-06-27T01:00:00+02:00"
image = "/images/blog/2018-06-27-recalbox-18-06-27/recalbox-18.06.27-banner.png"
title = "Recalbox 18.06.27"

[author]
  github = "https://github.com/substring"
  gitlab = "https://gitlab.com/substring45"
  name = "Substring"

+++

Aloha tout le monde !

Notre dernière version commence à dater un peu, et vous ne devinerez jamais pourquoi cette nouvelle version nous a pris autant de temps ... On a bossé dur sur une intégrée *à la Recalbox* ! Le mot **netplay** vous dit-il quelque chose ? Et bien oui, c'est à présent intégré de façon simple dans Recalbox !

Sans plus attendre, parcourons en détail le changelog :

* **Adding introRecalboxEASports.mp4**: Ambiance coupe du monde oblige, on s'est dit qu'une version Recalbox d'une célèbre intro de EA serait sympa ;)

* **ES: Added Netplay option in menus + menus to add roms hashs in gamelists**: un menu pour les bases de la configuration du Netplay

* **ES: Added Netplay : GUI for client mode and button to launch as host**: Tout ceci sera détaillé dans un wiki + blogpost dédié. En gros : une interface pour voir les parties en cours et facilement s'y connecter, avec des icônes explicites sur les chances de connexion. Et bien évidemment, démarrer une partie en mode serveur est tout aussi simple ! Gardez en tête que le bouton X (pad SNES) est celui qui vous mènera au netplay ! Quelques limites à retenir par contre : pas de netplay pour les consoles portables ou 3D + certains cores libretro sont trop exigeants (CPU/RAM) pour marcher sur Pi (ex: snes9x ne marchera pas en netplay à cause d'un manque de RAM)

* **ES: Fixed clock not in local time**: l'horloge était réglée sur GMT systématiquement. Plus maintenant.

* **ES: Fixed old themes crash**: celle-là elle est pour nous, ES plantait quand vous tentiez de revenir sur un thème à l'ancien format

* **ES: Option for popup position**: vous pouvez choisir dans quel coin de l'écran afficher la popup

* **ES: Help messages in popups**: les messages d'aide sont des popups à présent, et plus une boite de dialogue

* **ES: Slider for popup duration**: vous pouvez régler la durée d'une popup

* **ES: Languages names in their own language**: lors du choix de la langue, celle-ci est bien affichée dans la langue d'origine

* **ES: Refactored gamepad configuration panel**: la configuration des pads a été un peu simplifiée/revue

* **ES: Update Emulator / Core selection process to avoid gui inception and to display default values**: ça devrait être à présent plus simple de comprendre les paires emulateur/core (par défaut)

* **ES: Update font size on game metadata gui**: une taille de police réajustée

* **ES: Remove dead code of RomsManager**: suppression de code inutile dans ES, donc plus léger en RAM

* **ES: Make gamelist sort persistent**: les tris de gamelist sont à présent sauvegardés

* **ES: Show folder content in detail panel**: Un extrait du contenu des répertoires est à présent visible dans les gamelists

* **ES: Show single folder game as game in parent list**: permet de considérer ce dossier comme une ROM s'il n'y a qu'une ROM dedans. Pratique pour les dossiers contenant des éléments de CD-ROM

* **ES: Added an option to list folder content instead of folder name**: une option permet à présent de voir tous les jeux contenus dans des répetoires directement sans avoir à naviguer dedans

* **ES: Only display available letter in JumpToLetter + Append figures**: on ne liste que les lettres / chiffres vers lesquelles on peut naviguer rapidement

* **ES: Avoid reload of images if source doesn't changed**: simple optimisation d'affichage

* **ES: Update Games lists from GameList menu**: la mise à jour des jeux est dispo en appuyant sur SELECT une fois dans un système

* **ES: Set Menu list in loop mode**: on peut ENFIN aller du premier au dernier élément (et vice-versa) dans un menu !!!

* **ES: Fixed back button in gamelist**: on remonte bien d'un seul cran quand on navigue dans des sous-dossiers

* **ES: Fixed back from game launch**: en revenant d'une partie, la ROM lancée est toujours celle sélectionnée, même dans un sous-répertoire

* **ES: Fixed detailed panel missing when jumping to letter A**: il y avait un problème d'affichage lorsque l'on sautait vers le premier jeu de la liste, c'est résolu

* **ES: Fixed set a game favorite for the first time (required two switches before)**: il fallait parfois activer 2 fois l'option de favori pour qu'elle fonctionne, plus maintenant

* **ES: Stay is selected sub-folder when changing sort type / order in gamelist**: en triant une gamelist, on se retrouvait à la racine du système, mais c'est du passé à présent

* **Bump kodi-plugin-video-youtube to 5.5.1**: un plugin qu'on doit mettre à jour plus souvent que nécessaire

* **recalbox-config.sh: added getEmulatorDefaults**: un script qui nous aide à connaitre l'émulateur par défaut d'un système

* **Added fonts to support all languages**: on devrait enfin réussr à afficher toutes les langues sans problème

* **picodrive: partially rewrite package + re-enable for odroidc2**: Ô joie pour les possesseurs de C2 qui retrouvent enfin Picodrive !

* **Add alternative methods to detect sound cards**: une 3e méthode de détection des cartes son qui pourrait s'avérer utile pour de nouvelles cartes

* **Boot time optimization**: suppression de chargements un peu lents, réduisant le temps de chargement de Recalbox

* **Wifi: option to change region added to recalbox.conf - with wifi.region=FR fixes channel 12 issue for example**: en définissant le pays, on peut profiter d'autres canaux WiFi (12 et 13 en France avec le Wifi embarqué du pi3 par exemple)

* **Old deprecated API removed, new API in development**: la vieille API n'est plus maintenue depuis 2 ans et de toute façon ne marche plus depuis la 4.1

* **more informations in support archives**: c'est pour mieux vous aider, mon enfant !

* **sound: try to fix configuration loss on upgrade**: un bug trés agaçant surtout sur x86, où la configuration du son saute lors d'une mise à jour. Cette mise à jour sera la dernière qui devrait poser le problème ... j'espère ...

* **Add message to warn before turning off Recalbox**: certains éteignent leur Recalbox trop tôt. Donc un petit message qui dit de ne pas le faire

* **Add Daphne system using Hypseus emulator**: à vous les joies du Laserdic !

* **pin356ONOFFRESET: improved behaviour, more compatible with nespi+ case**: possesseurs d'un nespi+, réglez dans recalbox.conf pin356ONOFFRESET

* **Add splash screen duration option in recalbox.conf**: vous pouvez régler la durée de la vidéo d'intro : soit une durée fixe, soit jusqu'à ce qu'elle se termine

* **Bump mGBA emulator and add it to gb and gbc for Super Game Boy support**: vous pouvez utiliser l'émulateur mGBA (GameBoy et GameBoy Color) pour jouer aux roms compatibles en mode Super GameBoy de la Super Nintendo. Ça donne un effet sympatoche !

* **Add Thomson TO8(D) support with libretro core theodore, thank you zlika**: Le premier ordinateur que j'ai appris à utiliser et qui m'a fait me dire "Je veux faire çà dans la vie !"

* **linapple: support apple2.configfile=dummy to avoid overwriting configuration**: un moyen de désactiver la génération automatique de configuration pour l'Apple II

* **Add Amstrad CPC core: crocods**: un émulateur sans clavier virtuel mais qui marche bien

* **New version of libretro-o2em now supports save states and rewind**: Odyssey2 supporte les savestates et le rewind maintenant

* **Bump all libretro cores**: c'est plus sympa pour le netplay :D

* **Bump Retroarch to 1.7.3 + patch required parts**: obligé ! je vous recommande vivement de lire [le blog libretro](https://www.libretro.com/index.php/retroarch-1-7-3-released/) pour avoir toutes les infos. Certains auront remarqué qu'il est possible de jouer à 15kHz sur x86 ! On n'a pas eu le temps de tester de nous-même, mais faites nous vos retours sur le forum !

* **configgen: N64 back to fullscreen the real way, without forcing resolution**: jusque là, pour éviter un plantage, on devait simuler une fenêtre de la taille de l'écran. Maintenant, du vrai plein écran !

* **configgen: the video syntax "auto DMT 4 HDMI" now works for N64**: l'option de videomode "auto" marche à présent complètement sur N64, y compris pour forcer une résolution par défaut si supportée par le moniteur (pi seulement)

Quel changelog mes amis ! Je ne vous cache pas qu'on n'est pas peu fier de notre travail sur le netplay ! Nous espérons que vous aller profiter de cette nouvelle version et partagerez votre amusement avec nous ! Pensez à activer les retroachievements en netplay, ca marche aussi pour tous les joueurs ;)
