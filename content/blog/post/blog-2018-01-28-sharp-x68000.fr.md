+++
date = "2018-01-28T11:00:00+00:00"
image = "/images/blog/2018-01-28-sharp-x68000/recalbox-sharp-x68000-banner.png"
title = "SHARP X68000"

[author]
  github = "https://github.com/rockaddicted"
  gitlab = "https://gitlab.com/rockaddicted"
  name = "rockaddicted"
+++

Hello les amigos,

Il est temps d'accueillir un nouveau système dans Recalbox : **le SHARP X68000 !**  

Afin d'émuler ce système nous utilisons le [core libretro px68k](https://github.com/libretro/px68k-libretro) basé sur [l'émulateur px68k](https://github.com/hissorii/px68k) de Hissorii.  

Pour votre information, ce core nécessite l'ajout de nouveaux bios. Voici les fichiers [à ajouter dans votre Recalbox](https://github.com/recalbox/recalbox-os/wiki/Ajoutez-des-bios-%28FR%29) :  

- **7fd4caabac1d9169e289f0f7bbf71d8e** - **keropi/iplrom.dat**
- **cb0a5cfcf7247a7eab74bb2716260269** - **keropi/cgrom.dat**

Ces fichiers doivent être placés dans un **sous dossier** nommé "keropi".  

Si vous avez besoin de plus d'informations à propos de ce core, merci de lire la [documentation libretro](https://buildbot.libretro.com/.docs/library/px68k).  


Nous espérons que vous allez apprécier ce nouveau core et que vous allez (re)découvrir la ludothèque du SHARP X68000.

## Vidéo de présentation des 30 meilleurs jeux SHARP X68000

<iframe width="560" height="315" src="https://www.youtube.com/embed/zmIXovryWrw" frameborder="0" allowfullscreen></iframe>

