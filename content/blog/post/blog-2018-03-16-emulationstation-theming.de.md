+++
date = "2018-03-16"
image = "/images/blog/2018-03-16-emulationstation-theming/18.03.16_release-stable-visual.jpg"
title = "EmulationStation Theming"

[author]
  github = "https://github.com/Supernature2k"
  gitlab = "https://gitlab.com/Supernature2k"
  name = "Supernature2k"
[translator]
  github = "https://github.com/Nachtgarm"
  gitlab = "https://gitlab.com/Nachtgarm"
  name = "Nachtgarm"
+++

Hallo, Freunde des guten Geschmacks!

Jetzt, da wir ein zuverlässiges und aktuelles System haben, lasst uns ein paar kosmetische Verbesserungen mit einbringen.

Das Recalbox-Team ist stolz euch ein Update des **EmulationStation**-Frontends vorstellen zu können!

Die neue Version enthält einige Theme-Verbesserungen:

* Das System-Karussell ist jetzt vollständig anpassbar. Ihr könnt nun endlich die weißen Boxen hinter den Logos ändern oder entfernen und...


[![new carousel](/images/blog/2018-03-16-emulationstation-theming/new-carousel_tn.jpg)](/images/blog/2018-03-16-emulationstation-theming/new-carousel.png)


* Ihr könnt ein **vertikales Karussell** benutzen und es dort platzieren, wo ihr wollt!!!


[![standard](/images/blog/2018-03-16-emulationstation-theming/standard_tn.jpg)](/images/blog/2018-03-16-emulationstation-theming/standard.png)

[![vertical](/images/blog/2018-03-16-emulationstation-theming/vertical-carousel_tn.jpg)](/images/blog/2018-03-16-emulationstation-theming/vertical-carousel.png)


* Das Hilfesystem (die kleinen Icons und Infos am unteren Rand des Bildes) kann nun auch verändert werden!
* Ihr könnt auch Hintergrund, Farben, Schriftarten und vieles mehr in den Menüs ändern!


[![menus](/images/blog/2018-03-16-emulationstation-theming/menus_tn.jpg)](/images/blog/2018-03-16-emulationstation-theming/menus.png)

[![new menu](/images/blog/2018-03-16-emulationstation-theming/new-menu_tn.jpg)](/images/blog/2018-03-16-emulationstation-theming/new-menu.png)


Außerdem haben wir ein neues *Subset*-System in den Themes und eine *$system* Variable hinzugefügt:

Ihr könnt also ab sofort mehrere *Variationen* von verschiedenen Theme-Komponenten haben und sie direkt in **EmulationStation** auswählen.


[![theme config](/images/blog/2018-03-16-emulationstation-theming/theme-config_tn.jpg)](/images/blog/2018-03-16-emulationstation-theming/theme-config.png)

[![subset config](/images/blog/2018-03-16-emulationstation-theming/theme-config2_tn.jpg)](/images/blog/2018-03-16-emulationstation-theming/theme-config2.png)


* colorset: Nutzt in euren Themes so viele Farbvariationen, wie ihr mögt
* iconset: Wählt zwischen diversen Icon-Sets für das Hilfesystem
* menu: Macht verschiedene Menü-Themes in *ES* auswählbar
* systemview: Wählt eure bevorzugte System-Ansicht mit verschiedenen Karussell-Modi
* gamelist: wie oben
* region: Ihr könnt Tags für Theme-Elemente benutzen (EU/US/JP) und jene auswählen, die ihr anzeigen möchtet.

Die System-Variable ermöglicht es euch **eine** "theme.xml"-Datei für alle Systeme anzulegen!

Um euch alle Möglichkeiten zu zeigen, die euch das neue System bietet, haben wir das Recalbox-Theme mit vielen Optionen erweitert:

* iconsets für SNES/PSX/XBox Controller
* neue Menüs
* Viele Optionen für systemview. U.a. angepasst für Röhren und kleine oder vertikale Bildschirme


[![CRT](/images/blog/2018-03-16-emulationstation-theming/crt-jp_tn.jpg)](/images/blog/2018-03-16-emulationstation-theming/crt-jp.png)


* Diverse gamelist-Ansichten. U.a. Big-Picture Modus

Weitere Infos über **EmulationStation**-Themes und wie das neue Recalbox-Theme gestaltet ist, werden [*hier*](https://gitlab.com/recalbox/recalbox-emulationstation/blob/master/THEMES.md) (Englisch) folgen.