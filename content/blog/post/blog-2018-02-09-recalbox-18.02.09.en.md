+++
date = "2018-02-09T11:00:00+02:00"
image = "/images/blog/2018-02-09-recalbox-18-02-09/recalbox-18.02.09-banner.jpg"
title = "Recalbox 18.02.09"

[author]
  github = "https://github.com/substring"
  gitlab = "https://gitlab.com/substring45"
  name = "Substring"

+++

Hey guyz !

It's been a little while since the last release ! Time to enjoy holidays, get some rest and still work on some new features !

So, what to expect from this 18.02.09 ? Here is a list of major changes:

* Added new out of the box supported pads : ouya, micreal arcade dual and ipega9055
* A new refreshing splash video (thank you @ian57)
* x86 (32 and 64 bits) gets DS emulation with libretro cores desmume and melonds
* Mame2010 core is available for all boards but rpi0/1
* You can mount NAS shares with Wifi at boot now. Just configure the WiFi and the shares following the [wiki](https://github.com/recalbox/recalbox-os/wiki/Load-your-roms-from-a-network-shared-folder-%28EN%29#recalbox-version--41)
* Power scripts updated (when you use some GPIO buttons to control power/reset)
* Updated DOSBox and ScummVM
* New emulator: ResidualVM [homepage](http://www.residualvm.org). This can only run Grim Fandango, Escape from Monkey Island and Myst III. Be sure to carefully read the documentation at the ResidualVM website. To play those games, drop them in the **scummvm** roms folder with the `.residualvm` extension, as you would for a scummvm game
* Added sharp x68000 - px68k libretro core. I'm sure very few of you knew this system back then ;)
* Added 3DO for XU4 and x86 32/64

Hope you'll enjoy this new release !

And ... Oh my God ... Time to make that famous Steve Job's "One last thing" ...

Pi2 and Pi3 are the lucky ones to get Amiga support ! Yeah baby ! Thanks to our community member Voljega for his amazing work on Amiga for Recalbox for years, now it's definitely integrated inside Recalbox !