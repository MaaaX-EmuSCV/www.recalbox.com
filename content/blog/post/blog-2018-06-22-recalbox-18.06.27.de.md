+++
date = "2018-06-27T01:00:00+02:00"
image = "/images/blog/2018-06-27-recalbox-18-06-27/recalbox-18.06.27-banner.png"
title = "Recalbox 18.06.27"

[author]
  github = "https://github.com/substring"
  gitlab = "https://gitlab.com/substring45"
  name = "Substring"
[translator]
  github = "https://github.com/robsdedude"
  gitlab = "https://gitlab.com/robsdedude"
  name = "Robsdedude"
+++


Hallo allerseits!

Seit unserem letzten Release ist einige Zeit vergangen... Sicher könnt ihr erraten, warum die neue Version so lange auf sich warten ließ: Wir haben sehr hart an einem Feature gearbeitet, das auf die "Recalbox Weise" integriert wurde! Verdammt nochmal, ja! Ihr wisst bestimmt, was **netplay** bedeutet, oder? Ab jetzt ist es in ES in einer einfach zu bedienenden Oberfläche integriert!

Kommen wir zu den Neuerungen im Detail (Changelog):

* **introRecalboxEASports.mp4**: Die FIFA WM hat uns inspiriert eine Recalbox-Version des berühmten EA-Intros zu machen ;)

* **ES: Netplay-Optionen im Menu + Menüs um Rom-Hashes in die gamelists aufzunehmen**

* **ES: Netplay: Oberfläche für Client-Modus und Button um im Host-Modus zu starten**: Details zur Bediednung werden im Wiki und einem extra Netplay-Blog-Post veröffentlicht. Zusammengefasst: Es gibt ein hübsches Fenster, in dem existierende Netplay-Spiele mit Icons gelistet werden, die euch anzeigen, ob ihr beitreten könnt. Und natürlich könnt ihr auch ganz einfach ein Spiel als Server (Host-Modus) starten. Ihr müsst euch nur den X-Button merken (SNES Layout). Mit ihm erreicht ihr alles für Netplay relevante. Es gibt aber auch Einschränkungen: Netplay wird nicht für Handhelds oder 3D Konsolen unterstützt. Außerdem könnten manche Cores mit Netplay mehr Leistung benötigen, als ein Pi bietet (Snes9x z.B. benötigt mit Neplay mehr RAM, als der Pi3 hat).

* **ES: Uhrzeit wird jetzt in lokaler Zeit angezeigt**: Zuvor wurde die Zeit, unabhängig des Standortes, in GMT angezeigt.

* **ES: Alte Themes verursachen keinen Absturz mehr**: Unser Fehler, sorry. Themes nach den alten Spezifikationen brachten ES zum Abstürzen.

* **ES: Option für Pop-Up-Position**: Jetzt könnt ihr entscheiden, in welcher Ecke Pop-ups angezeigt werden!

* **ES: Hilfe-Nachrichten in Pop-ups**: Bisher waren Hilfe-Nachrichten Dialog-Boxen. Jetzt werden Sie in Pop-ups angezeigt.

* **ES: Schieberegler für Pop-up Dauer**: Ihr könnt selbst entscheiden, wie lange Pop-ups angezeigt werden.

* **ES: Sprach-Einstellungen in den jeweiligen Sprachen anzeigen**: selbsterklärend ;)

* **ES: Controller-Konfiguration neu gestaltet**: Die Einstellungen wurden modernisiert. Mehr dazu in einem extra Blog-Post.

* **ES: Neuer Ablauf beim Auswählen des Emulators/Cores verhindert GUI-Verschachtelung und Zeit Standard-Werte**: Leichter zu lesen und zu verstehen.

* **ES: Schriftgröße von Spiel-Metadaten geändert**

* **ES: Ungenutzter Code im RomsManager entfernt**: Weniger Code bedeutet weniger Speicher-Verbrauch.

* **ES: Sortierung der Spiele-Liste speichern**: Die eingestellte Sortierung der Spiele ging verloren, wenn man andere Einstellungen änderte und dann neustartete. Das ist jetzt behoben.

* **ES: Ordnerinhalte in Detail-Panel anzeigen**: Zuvor wurden beim Auswählen eines Ordners in der Spiele-Liste die Details des vorig gewählten Spiels angezeigt. Jetzt wird eine Vorschau der Spiele im ausgewählten Ordner angezeigt (mit Themes komplett anpassbar).

* **ES: Ordner mit nur einem Spiel werden als Spiel in der darüber liegenden Liste angezeit**: Das bedeutet ihr müsst z.B. bei PSX und Dreamcast nicht mehr die Ordner öffnen um die Spiele sehen und starten zu können.

* **ES: Option um Ordnerinhalte statt deren Namen anzuzeigen**: Diese Option ermöglicht das Anzeigen aller Spiele in komplexen Ordnerstrukturen in nur einer Liste ohne dabei die Ordner selbst zu sehen.

* **ES: Zu Buchstaben Springen zeigt nur vorhandene Zeichen an**: Die "Springe zu Buchstaben" Funktion zeigt nur noch vorhandene Buchstaben und Ziffern.

* **ES: Bilder nur neu laden, wenn sie sich geändert haben**: Eine kleine Optimierung der Anzeige

* **ES: Spiele-Liste kann vom Menü der Spiele-Liste aktualisiert werden**: Diese Feature ist damit nun direkt im System verfügbar.

* **ES: Menüs springen im Kreis**: Springt direkt zum Ende der Menü-Einträge, indem ihr beim ersten Eintrag nach oben drückt. Ein Klassiker. Schade, dass das erst jetzt funktioniert.

* **ES: Zurück-Button in der Spiele-Liste repariert**: Bisher bringt euch der dieser in den obersten Ordner. Ab jetzt springt ihr nur noch einen Ordner nach oben.

* **ES: Rückkehr von Spielen repariert**: Zuvor fandet ihr euch nach dem Beenden eines Spiels im obersten Ordner wieder. Jetzt bleibt ihr in dem vorigen Ordner.

* **ES: Detail-Panel auch beim Sprung zu A anzeigen**: Zuvor wurden die Details nicht aktualisiert, wenn man zum ersten Spiel sprang. Das ist jetzt behoben.

* **ES: Fehlerhaftes Verhalten beim favorisieren von Spielen behoben**: In manchen Spiele-Listen musste man beim ersten Mal den Favoritenstatus doppelt wechseln, um ein Spiel zu den Favoriten hinzuzufügen.

* **ES: Im aktuellen Ordner bleiben, wenn die Spiele-Liste umsortiert wird**: Zuvor sprang man zum obersten Ordner, wenn man die Spiele-Liste umsortierte.

* **kodi-plugin-video-youtube auf Version 5.5.1 aktualisiert**: Dieses Plugin entwickelt sich sehr schnell aber nur die letzten paar Versionen werden bereitgestellt. Daher mussten wir updaten.

* **recalbox-config.sh: getEmulatorDefaults hinzugefügt**: Eine Hilfsfunktion, die Entwicklern ermöglicht den System-Default-Emulator zu ermitteln.

* **Neuer Font, um alle Sprachen zu unterstützen**: Es sollten jetzt alle Sprachen der Welt ordentlich angezeigt werden können.

* **picodrive: Paket teilweise neu geschrieben und wieder für odroidc2 aktiviert**: C2 Nutzer können sich freuen. Für sie ist picodrive wieder verfügbar.

* **Alternative zur Erkennung von Sound-Karten hinzugefügt**: Das hat vorerst keine Auswirkungen, erleichtert aber das Hinzufügen von Boards, die ihre Sound-Karten nicht wie andere angeben.

* **Zeit beim Hochfahren verkürzt**: Ein langsamer Timeout wurde ersetzt. ES wir jetzt schneller anfangen zu laden.

* **WLAN: Region-Option zu recalbox.conf hinzugefügt - wifi.region=FR behebt z.B. Probleme mit Kanal 12**: Nutzer in Frankreich werden wissen, was das bedeutet: Wir sind nicht mehr auf die US-amerikanischen Kanäle beschränkt, sondern können jetzt auch die Kanäle 12 und 13 des internen WLAN-Chips des Pi3 benutzen!

* **Alte API entfernt, neue bereits in Entwicklung**: Die alte API wird seit 2 Jahren nicht mehr weiterentwickelt und funktioniert seit Version 4.1 nicht mehr. Daher wurde sie entfernt.

* **Mehr Infos in den Support-Archieven**: Das hilft uns euch einen besseren Support zu bieten.

* **Sound: Versuch den Verlust von Einstellungen beim Update zu beheben**: Dieser Bug verursachte bei PC-Nutzern den Verlust der Ton-Einstellungen bei jedem Update. Das sollte bei diesem Update das letzte mal passieren.

* **Warnung beim Ausschalten von Recalbox**: Manche Nutzer haben ihre Recalbox zu früh abgeschaltet, was zu korrupten Daten führte. Ab jetzt gibt es wenigstens eine Nachricht, die Nutzer davor warnt das Gerät zu früh auszuschalten.

* **Daphne-System via Hypseus-Emulator hinzugefügt**: Genießt LaserDisc Spiele!

* **pin356ONOFFRESET: Verbessertes Verhalten, bessere Kompatibilität mit nespi+ Gehäuse**: Setzt den powerswtich Typ einfach auf pin356ONOFFRESET, wenn ihr euer nespi+ Gehäuse verwenden wollt.

* **Dauer des Splash-Screens kann in recalbox.conf eingestellt werden**: Ihr könnt eurer Recalbox jetzt sagen, wie lange sie die Splash-Videos anzeigen soll: Wie bisher für eine fixe Zeit, oder bis zum Ende des Videos (das wird den Start von ES verzögern).

* **mGBA aktualisiert und zu GB und GBC hinzugefügt, um Super Game Boy zu unterstützen**: mGBA kann GameBoy und GameBoy Color ROMs im Super GameBoy Format abspielen. Der Super GameBoy ist eine SNES-Cartridge mit der man GameBoy-Spiele auf dem SNES spielen kann.

* **Thomson TO8(D) Unterstützung mit dem theodore Core von libretro, danke an zlika**: Der erstem Computer, den ich in meinem Leben benutzt habe. Seither wusste ich, dass ich in der IT-Branche arbeiten würde, wenn ich mal groß bin!

* **lineapple: apple2.configfile=dummy kann verwendet werden, um das Überschreiben von Konfigurationen zu verhindern**: So könnt ihr Recalbox konfigurieren keine lineapple Konfigurationsdateien zu erzeugen.

* **Amstrad CPC Core: crocods**: Es gibt hierfür zwar keine virtuelle Tastatur, aber sonst funktioniert es gut.

* **Neue Version von libretro-o2em bietet jetzt Save-States und Zurückspulen**

* **Alle libretro Cores aktualisiert**: Das bietet euch bessere Chancen Mitspieler bei Netplay zu finden.

* **Retroarch auf Version 1.7.3 aktualisiert und notwendige Patches eingepflegt**: Notwendig! Wir empfehlen euch [den libretro blog (en)](https://www.libretro.com/index.php/retroarch-1-7-3-released/) zu lesen. Dort findet ihr alle Details. Ein paar Leute haben herausgefunden, dass mit den richtigen Einstellugnen auch 15kHz PC-Bildschirme verwendet werden können. Bisher hatten wir aber noch keine Zeit damit herumzuexperimentieren. Das werden wir aber so bald wie möglich nachholen! Gebt uns im Forum gerne Feedback dazu!

* **configgen: N64 wieder im Fullscreen ohne erzwungene Auflösung**: Bisher mussten wir ein Fenster mit der Größe des Bildschirmes simulieren, um einen Audio-Bug zu umgehen. Dank mupen64plus können wir jetzt auf den richtigen Fullscreen-Modus zurückgreifen.

* **configgen: Die Video Syntax "auto DMT 4 HDMI" funktioniert jetzt für N64**: Die neue, für `auto` spezifische Option eine Auflösung nur dann zu verwenden, wenn euer Monitor diese auch unterstützt (Feature nur für Pi) funktioniert jetzt auch für N64.


Das ist eine laaanger Changelog und wir sind besonders stolz auf Netplay! Wir hoffen ihr habt Spaß mit dem neuen Release und teilt eure Erfahrungen.  
Wir empfehlen euch retroachivements in Kombination mit Netplay. Sie sind kompatibel.
