+++
date = "2017-12-01T16:00:00+02:00"
image = "/images/blog/2017-11-29-recalbox-17-12-01/recalbox-17.12.01-banner.png"
title = "Recalbox 17.12.01"

[author]
  github = "https://github.com/rockaddicted"
  gitlab = "https://gitlab.com/rockaddicted"
  name = "rockaddicted"
[translator]
  github = "https://github.com/robsdedude"
  gitlab = "https://gitlab.com/robsdedude"
  name = "Robsdedude"
+++

Hallo ihr Lieben!

Heute ist es an der Zeit die neue Recalbox Version **17.12.01** zu veröffentlichen!

Schauen wir uns die neuen Features ein Mal genauer an:

* Viele Emulatoren wurden aktualisiert: Retroarch auf **1.6.9**, ScummVM auf **1.10.0** "unstable", DosBox auf **0.74 r4063** (danke [lmerckx](https://gitlab.com/lmerckx)) und **alle libretro-Cores** auf die neusten Versionen. Mehr Information findet ihr in diesem [dedizierten Blog-Eintrag]({{<ref "blog-2017-11-17-bump-emulators.de.md">}})
_Zur Info: Der **fba-libretro** Core basiert nun auf fba **v0.2.97.42**._
_Vergesst also nicht, euer Rom-Set zu aktualisieren, bevor ihr spielt. Die neuen **.dat**-Dateien_ [gibt es hier](https://gitlab.com/recalbox/recalbox/tree/master/package/recalbox-romfs/recalbox-romfs-fba_libretro/roms/fba_libretro/clrmamepro)!
* Mit Recalbox auf x86 und x86_64 könnt ihr nun mit den **mednafen_psx**-Cores die PSX emulieren! Mehr darüber in diesem [dedizierten Blog-Eintrag]({{<ref "blog-2017-11-11-x86-mednafen-psx.de.md">}}).
* Der Dolphin-Emulator unterstützt nun Hotkey-Kombinationen und Display-Einstellungen. Außerdem haben wir die **Wiimote-Emulation** verbessert.
* Mit **imlib2_grab** könnt ihr auf x86 und x86_64 nun Screenshots von der Konsole aus machen!
* Das Boot-Menü auf x86 und x86_64 (GRUB) hat nun eine **verbose** Option, die euch und uns beim Problemfinden hilft.
* Des Weiteren haben wir einen Fehler bei der Konfiguration mehrerer WiFi-Netzwerke behoben. Vielen Dank [OyyoDams](https://gitlab.com/OyyoDams)
* Auf Odroid C2 und XU4 könnt ihr nun mit dem mupen64plus **GLideN64 Video-Plugin** N64-Spiele spielen (Standard auf XU4).
* Weitere Punkte könnt ihr im [Changelog](https://gitlab.com/recalbox/recalbox/blob/master/CHANGELOG.md) (englisch) nachlesen, welches euch nach dem Update eurer Recalbox angezeigt wird.


Viele weitere, coole Features werden bald kommen.
Folgt uns weiter und habt Spaß mit der neuen Version ;)
