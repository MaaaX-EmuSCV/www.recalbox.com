+++
date = "2018-02-09T11:00:00+00:00"
image = "/images/blog/2018-02-09-amiga/recalbox-amiga-banner.png"
title = "Amiga"

[author]
  github = "https://github.com/voljega"
  gitlab = "https://gitlab.com/voljega"
  name = "Voljega"
+++

Hello everyone !

After a long test campaign, Amiga emulation is finally integrated into Recalbox !

Here are the details about the functionnality:

* The emulator used is [Amiberry version 2.1](https://github.com/midwan/amiberry).
* 2 systems are supported for now: Amiga 500/600 (both in the a600 system) and Amiga 1200. Amiga CD32 needs a few evolutions on Amiberry's side (future version 2.5) and will be integrated later.
* Supported formats are disks (.adf, automatic loading of multiple disks) and hard drive (WHDL).

You need at least 1 BIOS file depending on the system you want to emulate:

* Amiga 600 ADF  **82a21c1890cae844b3df741f2762d48d  kick13.rom**
* Amiga 600 WHDL **dc10d7bdd1b6f450773dfb558477c230  kick20.rom**
* Amiga 1200 (both ADF and WHDL) **646773759326fbac3b2311fd8c8793ee  kick31.rom**

For more information, please consult the related readme.txt in roms folders and the dedicated [wiki page](https://github.com/recalbox/recalbox-os/wiki/Amiga-on-Recalbox-%28EN%29).

A thousand thanks to Ironic and Wulfman for their great investment in helping and testing !

Here is a small video to remind you of some of the best games :

<iframe width="560" height="315" src="https://www.youtube.com/embed/SrO9iJp8e2g" frameborder="0" allowfullscreen></iframe>