+++
date = "2017-03-04T16:36:09+02:00"
image = "/images/blog/2017-03-04-favicons/favicons_feature-visual.jpg"
title = "New Favicons!"
draft = false

[author]
  github = "https://github.com/rockaddicted"
  gitlab = "https://gitlab.com/rockaddicted"
  name = "rockaddicted"
  
+++


<p dir="auto">Hi, today let's see a small but cool improvement about the favorites system.</p><p dir="auto">Favorites system is a very good feature introduced in 4.0.0 release. But, when you have many games tagged as favorites, or many games with the same name in different game systems, the global favorites system can become messy.</p><p dir="auto">So, we decided to replace the standard <img src="/images/blog/2017-03-04-favicons/star.png" height="20" width="20" align="absmiddle"> icon with <strong>specific icon for each systems</strong>.</p><p dir="auto">You can see here examples with the GBA system and the final result in Favorites System:</p><p dir="auto"></p><div class="image-container"><a class="no-attachment-icon" href="https://www.recalbox.com/assets/images/blog/2017-03-04-favicons/es_favicons_2.jpg" target="_blank"><img src="/images/blog/2017-03-04-favicons/es_favicons_2.jpg" alt="es_favicons_2"></a></div><div class="image-container"><a class="no-attachment-icon" href="https://www.recalbox.com/assets/images/blog/2017-03-04-favicons/es_favicons.jpg" target="_blank"><img src="/images/blog/2017-03-04-favicons/es_favicons.jpg" alt="es_favicons"></a></div><p dir="auto">Here is a short demo video :</p><iframe width="560" height="315" src="https://www.youtube.com/embed/le9c8dVtf8I" frameborder="0" allowfullscreen=""></iframe>