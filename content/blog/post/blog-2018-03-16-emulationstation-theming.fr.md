+++
date = "2018-03-16"
image = "/images/blog/2018-03-16-emulationstation-theming/18.03.16_release-stable-visual.jpg"
title = "EmulationStation Theming"

[author]
  github = "https://github.com/Supernature2k"
  gitlab = "https://gitlab.com/Supernature2k"
  name = "Supernature2k"

+++


Bonjour à tous !

Maintenant que nous avons un système fiable et à jour, apportons de sympathiques améliorations cosmétiques !

L'équipe Recalbox est fière de vous présenter un **EmulationStation** mis à jour !


Cette version introduit de nombreuses améliorations de theming :

* Le carousel système est désormais entièrement themable ! Vous pouvez enfin changer ou enlever les bandes blanches derrière les logos et ...


[![new carousel](/images/blog/2018-03-16-emulationstation-theming/new-carousel_tn.jpg)](/images/blog/2018-03-16-emulationstation-theming/new-carousel.png)


* Vous pouvez avoir un **carousel vertical** et le placer où vous voulez !!!


[![standard](/images/blog/2018-03-16-emulationstation-theming/standard_tn.jpg)](/images/blog/2018-03-16-emulationstation-theming/standard.png)

[![vertical](/images/blog/2018-03-16-emulationstation-theming/vertical-carousel_tn.jpg)](/images/blog/2018-03-16-emulationstation-theming/vertical-carousel.png)



* Le système d'aide (les petites icônes au bas de l'écran) et aussi themable maintenant !
* Vous pouvez aussi changer le fond, les couleurs, les polices et bien plus dans les menus !



[![menus](/images/blog/2018-03-16-emulationstation-theming/menus_tn.jpg)](/images/blog/2018-03-16-emulationstation-theming/menus.png)

[![new menu](/images/blog/2018-03-16-emulationstation-theming/new-menu_tn.jpg)](/images/blog/2018-03-16-emulationstation-theming/new-menu.png)



Nous avons aussi introduit un nouveau système de *subset* dans les thèmes et une variable *$system*:

Vous pouvez avoir de nombreuses *variations* de différents composants du thème et choisir directement dans **EmulationStation**


[![theme config](/images/blog/2018-03-16-emulationstation-theming/theme-config_tn.jpg)](/images/blog/2018-03-16-emulationstation-theming/theme-config.png)

[![subset config](/images/blog/2018-03-16-emulationstation-theming/theme-config2_tn.jpg)](/images/blog/2018-03-16-emulationstation-theming/theme-config2.png)



* colorset: ajoutez autant de variations de couleurs que vous souhaitez pour votre thème
* iconset: choisissez parmi différents sets d'icônes pour le système d'aide
* menu: créez différents thèmes de menu sélectionnables depuis *ES*
* systemview: choisissez votre vue système préférée avec différents modes carousel
* gamelist: comme au-dessus
* région: vous pouvez ajouter un tag aux éléments de thème (EU/US/JP) et sélectionner ceux qui sont affichés!

La variable système vous permet de créer UN "theme.xml" pour tous les systèmes !

Pour vous montrer toutes les possibilités de cette évolution, nous avons mis à jour le thème recalbox avec de nombreuses options:

* iconsets pour les manettes SNES/PSX/XBox
* nouveaux menus
* de nombreuses vues système dont certaines adaptées aux petits écrans, cathodiques et écrans verticaux



[![CRT](/images/blog/2018-03-16-emulationstation-theming/crt-jp_tn.jpg)](/images/blog/2018-03-16-emulationstation-theming/crt-jp.png)



* différentes vues gamelist dont une vue "big picture"


Vous trouverez plus d'informations sur le theming d'**EmulationStation** [*ici*](https://gitlab.com/recalbox/recalbox-emulationstation/blob/master/THEMES.md) et vous pouvez regarder comment le nouveau thème recalbox est construit.
