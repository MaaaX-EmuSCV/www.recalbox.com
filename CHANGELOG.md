# Changelog
All notable changes to this project will be documented in this file.

* fix(recalstore): fix url when clicking on an item browsing on mobile
* chore(i18n): use .Site.Home.AllTranslations variable for available languages 
* chore(i18n): set english as default language on / and use netlify redirects
* fix(sitemap): use baseUrl to create valid sitemaps
* feat(recalstore): enable force item image in store
* feat(seo): remove broken link kit press
* chore(ci): bumped to hugo 0.55.1
* fix(store): fix product image display setting its parent div height
* feat(recalstore): highlight current store with bold font
* feat(recalstore): use store.recalbox.com as proxy for amazon
* perf(img): reduce drasticaly the size of the screen for homepage
* feat(seo): added title and description for all static pages
* feat(seo): added default description and keywords when not overriden
* fix(index.html): center features blocks on mobile
* fix(index.html): compress rewind gif from 450ko to 100ko, still fat but better
* feat(responsive-img): add responsive image partial and use it in our static pages
* doc(docker): add Docker with build instructions
* lang(pt-br): added pt-br lang
* lang(de): added de lang
* feat(home): change homepage promo message
* ci(deploy): build and deploy when pushing on gitlab
* feat(blog): blog integration
* feat(store): add store page, wip, still proxy to implement
* feat(pagefaq): add faq page
* feat(pagediy): add diy page
* feat(pagemedia): add media page
* feat(hugo): create home page with hugo
* feat(index.html): email to gitlab service desk
