# Recalbox website

This is the [Hugo] source code for the [Recalbox] website, store and blog.

## 🐳 Run website locally

First, build the Docker image:

```shell
docker build . -t recalbox/hugo
```

The website can then be run locally with the following command:

```shell
docker run -it --rm --name recalbox-hugo -v $(pwd):/src -p 1313:1313 recalbox/hugo
```

Then, head to http://localhost:1313 and enjoy live reloading while you edit your content ✨

### Using Docker Compose

Build the image with:

```shell
docker-compose build
```

Then, run the website locally by simply running:

```shell
docker-compose up
```

#### Run any [Hugo] command

Any arbitrary [Hugo] command can also be run, by prefixing it with `docker-compose run `, like:

```shell
docker-compose run hugo version
docker-compose run hugo new posts/my-new-post.en.md
```

> 📚 For more information, run:
> ```shell
> docker-compose run hugo --help
> ```

## 🌐 I18n

**Translation keys** in layouts are managed in TOML files in the [`i18n` directory](i18n).

Those translation keys can then be used from any content file using the `i18n` function in a Go template:

```
<p>{{ i18n "someKey" }}</p>
```

**Translated content** (blog posts, mostly) is achieved by prepending a locale code to the content file extension, _e.g._:

```
content/posts/my-post.en.md     # will be the English version
content/posts/my-post.fr.md     # will be the French version
```

We also encourage translators to give themselves credits in the translated post frontmatter, _e.g._:

```toml
[author]
  github = "https://github.com/rockaddicted"
  gitlab = "https://gitlab.com/rockaddicted"
  name = "rockaddicted"

[translator]
  github = "https://github.com/digitallumberjack"
  gitlab = "https://gitlab.com/digitallumberjack"
  name = "digitalLumberjack"
  twitter = "https://twitter.com/digitalumberjak"
```

> 📚 See Hugo's [Multilingual Mode] documentation for more information.

### Linking blog posts
If you want to link to a blog post, use Hugo's `ref` or `relref` function. Documentation can be found [here](https://gohugo.io/content-management/cross-references/).
Here are some examples:

* Link blog post in the same language:  
  `[read this blog post!]({{<ref blog-YYYY-mm-dd-TITLE.en.md>}})`
* Link a blog post in another language:  
  `[read this blog post in french!)({{<ref path="blog-YYYY-mm-dd-TITLE.fr.md" lang="fr">}})`


This has multiple advantages over regular markdown links:

* Hugo will complain if the link is broken.
* The routing is left to Hugo in case we decide to change the URL scheme.

### Adding support for a new language

* Add your language to `config.toml`. You can use existing languages as template.
* Go to `themes/recalbox-theme/static/img/flags` and follow the instructions of the `README` there.
* Go to [`i18n` directory](i18n) and copy `en.toml` (or any other language you want to translate from) to `<lang>.toml` where `<lang>` it the language shortcut (e.g. `en`) you used for the flag as well. Then edit the new file and translate all strings in quotes.
* Repeat last step, but in the `themes/recalbox-theme/i18n` directory.

[Hugo]: https://gohugo.io
[Recalbox]: https://www.recalbox.com
[Docker]: https://www.docker.com
[Multilingual Mode]: https://gohugo.io/content-management/multilingual


## Responsive images
Responsive images are mandatory to enhance site speed.

When writing pages in layout (static pages like home or media) you can use the `img.html` **partial** to make images responsives :
```hugo
{{ partial "img.html" (dict "src" "images/medias/recalstick.jpg") }}
```

When writing a blogpost, you can use the `img` **shortcode**:
```
[{{< img src="/images/blog/2018-06-27-netplay/lobby.png" alt="lobby">}}](/images/blog/2018-06-27-netplay/lobby.png)
```